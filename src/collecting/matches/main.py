from scrapy import cmdline


def run_scraping():
    # type: () -> None
    """
    Scraps all the matches to SQLite DB.

    :rtype: None
    :return: None.
    """
    command = "scrapy crawl matches"
    cmdline.execute(command.split())


if __name__ == "__main__":
    run_scraping()
