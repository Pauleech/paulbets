# -*- coding: utf-8 -*-
from dateutil import parser
from itertools import groupby
from scrapy.exporters import JsonItemExporter


def get_formation(values):
    # type: (list) -> str
    """
    Returns team formation for a given list of positions.

    :rtype: str
    :param values: team positions.
    :return: team formation.
    """
    groups = groupby(values)
    formation = [str(sum(1 for _ in group)) for label, group in groups][1::2]
    output = "-".join(formation)
    return output


def get_squad_splits(values):
    # type: (list) -> list
    """
    Returns team squad splits list of tuples (the way how to split squad on categories).

    :rtype: list
    :param values: team positions.
    :return: team squad splits list of tuples.
    """
    groups = groupby(values)
    formation = [sum(1 for _ in group) for label, group in groups][1::2]
    output = [(0, 1)]
    for f in formation[1:]:
        prev = output[-1][1]
        output.append((prev, prev + f))
    return output


class JsonPipeline(object):
    def __init__(self):
        # region predefined lists
        self.str_to_int = ["match_id", "home_team_id", "home_team_goal",
                           "away_team_id", "away_team_goal"]
        self.home_away = ["home", "away"]
        self.categories = ["keeper", "defenders", "midfielders", "forwards"]
        # endregion
        self.file = open("data.json", "wb")
        self.exporter = JsonItemExporter(self.file, encoding="utf-8", ensure_ascii=False)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        # type: (self, spider) -> None
        """
        Closes spider.

        :rtype: None
        :param spider: spider.
        :return: None.
        """
        self.exporter.finish_exporting()
        self.file.close()

    def convert_fields(self, item):
        # type: (self, Match) -> Match
        """
        Converts fields and returns match object.

        :rtype: Match
        :param item: match object
        :return: processed match object.
        """
        for key in self.str_to_int:
            item[key] = int(item[key])
        item["date"] = parser.parse(item["date"]).strftime("%Y-%m-%d")

    def convert_squads(self, item):
        # type: (self, Match) -> Match
        """
        Converts squads and returns match object.

        :rtype: Match
        :param item: match object
        :return: processed match object.
        """
        for key in self.home_away:
            # {}: "home", "away"
            item["{}_team_formation".format(key)] = get_formation(item["{}_players_pos".format(key)])
            spl = get_squad_splits(item["{}_players_pos".format(key)])
            for i, cat in enumerate(self.categories):
                # {}: "home", "away"; {}: "keeper", "defenders", "midfielders", "forwards"
                item["{}_{}".format(key, cat)] = item["{}_players".format(key)][spl[i][0]:spl[i][1]]

    def process_item(self, item, spider):
        # type: (self, Match, spider) -> Match
        """
        Processes and returns match object.

        :rtype: Match
        :param item: match object
        :param spider: spider.
        :return: processed match object.
        """
        self.convert_fields(item)
        self.convert_squads(item)
        self.exporter.export_item(item)
        return item
