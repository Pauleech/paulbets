# -*- coding: utf-8 -*-
import scrapy


class Match(scrapy.Item):
    # region main fields
    country = scrapy.Field()
    league = scrapy.Field()
    season = scrapy.Field()
    stage = scrapy.Field()
    match_id = scrapy.Field()
    date = scrapy.Field()
    # endregion
    # region teams fields
    home_team = scrapy.Field()
    away_team = scrapy.Field()
    home_team_acronym = scrapy.Field()
    away_team_acronym = scrapy.Field()
    home_team_id = scrapy.Field()
    away_team_id = scrapy.Field()
    home_team_goal = scrapy.Field()
    away_team_goal = scrapy.Field()
    home_team_formation = scrapy.Field()
    away_team_formation = scrapy.Field()
    # endregion
    # region players fields
    home_players = scrapy.Field()
    away_players = scrapy.Field()
    home_players_id = scrapy.Field()
    away_players_id = scrapy.Field()
    home_players_pos = scrapy.Field()
    away_players_pos = scrapy.Field()
    # endregion
    # region players categories fields
    home_keeper = scrapy.Field()
    home_defenders = scrapy.Field()
    home_midfielders = scrapy.Field()
    home_forwards = scrapy.Field()
    away_keeper = scrapy.Field()
    away_defenders = scrapy.Field()
    away_midfielders = scrapy.Field()
    away_forwards = scrapy.Field()
    home_keeper_id = scrapy.Field()
    home_defenders_id = scrapy.Field()
    home_midfielders_id = scrapy.Field()
    home_forwards_id = scrapy.Field()
    away_keeper_id = scrapy.Field()
    away_defenders_id = scrapy.Field()
    away_midfielders_id = scrapy.Field()
    away_forwards_id = scrapy.Field()
    # endregion
