import scrapy
from matches.items import Match


class MatchSpider(scrapy.Spider):
    name = "matches"
    allowed_domains = ["football-data.mx-api.enetscores.com", "json.mx-api.enetscores.com"]
    start_urls = ["http://football-data.mx-api.enetscores.com/page/xhr/standings/"]

    def __init__(self):
        super(MatchSpider, self).__init__()
        # self.enet_countries = ["England"]
        # self.enet_leagues = {"England": ["England Championship"]}
        # self.enet_seasons = ["2014/2015"]
        self.debug = False
        self.enet_countries = ["England", "Germany", "Italy", "France", "Spain",
                               "Belgium", "Netherlands", "Portugal"]
        self.enet_leagues = {"England": ["Premier League", "Championship"],
                             "Germany": ["1. Bundesliga"],
                             "Italy": ["Serie A"],
                             "France": ["Ligue 1"],
                             "Spain": ["Primera Division"],
                             "Belgium": ["First Division A"],
                             "Netherlands": ["Eredivisie"],
                             "Portugal": ["Primeira Liga"]}
        self.enet_seasons = ["2017/2018", "2016/2017", "2015/2016", "2014/2015", "2013/2014", "2012/2013"]

    def parse(self, response):
        # type: (self, response) -> request
        """
        Yields requests into countries crawl.

        :rtype: request
        :param response: initial response.
        :return: country request.
        """
        for country in self.enet_countries:
            href = response.xpath('//li[text()[contains(.,"{}")]]'
                                  "/@data-snippetparams".format(country)).re_first('"params":"(.+)"')
            url = "http://football-data.mx-api.enetscores.com/page/xhr/standings/{}".format(href)
            yield scrapy.Request(url, callback=self.parse_league, meta={"country": country})

    def parse_league(self, response):
        # type: (self, response) -> request
        """
        Yields requests into leagues crawl.

        :rtype: request
        :param response: country response.
        :return: league request.
        """
        country = response.meta["country"]
        for league in self.enet_leagues[country]:
            href = response.xpath('//li[text()[contains(.,"{}")]]'
                                  "/@data-snippetparams".format(league)).re_first('"params":"(.+)"')
            url = "http://football-data.mx-api.enetscores.com/page/xhr/standings/{}".format(href)
            yield scrapy.Request(url, callback=self.parse_season, meta={"country": country, "league": league})

    def parse_season(self, response):
        # type: (self, response) -> request
        """
        Yields requests into seasons crawl.

        :rtype: request
        :param response: league response.
        :return: season request.
        """
        country = response.meta["country"]
        league = response.meta["league"]
        for season in self.enet_seasons:
            href = response.xpath('//li[text()[contains(.,"{}")]]'
                                  "/@data-snippetparams".format(season)).re_first('"params":"(.+)"')
            url = "http://football-data.mx-api.enetscores.com/page/xhr/standings/{}".format(href)
            yield scrapy.Request(url, callback=self.parse_matches_init,
                                 meta={"country": country, "league": league, "season": season})

    def parse_matches_init(self, response):
        # type: (self, response) -> request
        """
        Yields request into matches init page crawl.

        :rtype: request
        :param response: season response.
        :return: matches init page request.
        """
        country = response.meta["country"]
        league = response.meta["league"]
        season = response.meta["season"]
        href = response.xpath('//div[contains(@class,"mx-matches-finished-betting_extended")]'
                              "/@data-params").re_first('params":"(.+)/')
        url = "http://football-data.mx-api.enetscores.com/page/xhr/stage_results/{}".format(href)
        first_stage_url = "{}/1".format(url)
        yield scrapy.Request(first_stage_url, callback=self.parse_stage,
                             meta={"country": country, "league": league, "season": season, "href": href})

    def parse_stage(self, response):
        # type: (self, response) -> request
        """
        Yields requests into matches stages crawl.

        :rtype: request
        :param response: matches init page response.
        :return: matches stage request.
        """
        country = response.meta["country"]
        league = response.meta["league"]
        season = response.meta["season"]
        href = response.meta["href"]
        url = "http://football-data.mx-api.enetscores.com/page/xhr/stage_results/{}".format(href)
        num_pages = response.xpath('//span[contains(@class,"mx-pager-next")]'
                                   "/@data-params").re_first('total_pages": "([0-9]+)"')
        stages = range(1, int(num_pages) + 1)
        if self.debug:
            stages = (1,)
        for stage in stages:
            full_stage_url = "{}/{}".format(url, stage)
            yield scrapy.Request(full_stage_url, callback=self.parse_matches, dont_filter=True,
                                 meta={"stage": stage, "country": country, "league": league, "season": season})

    def parse_matches(self, response):
        # type: (self, response) -> request
        """
        Fills in matches first info into new objects and yields requests into matches general crawl.

        :rtype: request
        :param response: matches stage response.
        :return: match general request.
        """
        country = response.meta["country"]
        league = response.meta["league"]
        season = response.meta["season"]
        stage = response.meta["stage"]
        matches_id = response.xpath('//a[@class="mx-link mx-hide mx-translate-attributes"]/@data-event').extract()
        dates = response.xpath('//span[@class="mx-time-startdatetime"]/text()').extract()
        if self.debug:
            matches_id = (matches_id[0],)
        for i, match_id in enumerate(matches_id):
            match = Match()
            match["match_id"] = match_id
            match["country"] = country
            match["league"] = league
            match["season"] = season
            match["stage"] = stage
            match["date"] = dates[i]
            url = "http://football-data.mx-api.enetscores.com/page/xhr/match_center/{}/".format(match_id)
            yield scrapy.Request(url, callback=self.parse_general_stats, meta={"match": match})

    def parse_general_stats(self, response):
        # type: (self, response) -> request
        """
        Fills in match general info into object and yields request into match squad crawl.

        :rtype: request
        :param response: match general response.
        :return: match squad request.
        """
        match = response.meta["match"]
        match_id = match["match_id"]
        for state in ["home", "away"]:
            team_full_name = response.xpath('//div[@class="mx-team-{}-name mx-break-small"]'
                                            "/a/text()".format(state)).re_first("\t+([^\n]+[^\t]+)\n+\t+")
            team_id = response.xpath('//div[@class="mx-team-{}-name mx-break-small"]'
                                     "/a/@data-team".format(state)).extract_first()
            team_acronym = response.xpath('//div[@class="mx-team-{}-name mx-show-small"]'
                                          "/a/text()".format(state)).re_first("\t+([^\n]+[^\t]+)\n+\t+")
            team_goal = response.xpath('//div[@class="mx-res-{} "]'
                                       "/@data-res".format(state)).extract_first()
            match["{}_team".format(state)] = team_full_name
            match["{}_team_acronym".format(state)] = team_acronym
            match["{}_team_id".format(state)] = team_id
            match["{}_team_goal".format(state)] = team_goal
        url = "http://football-data.mx-api.enetscores.com/page/xhr/event_gamecenter/{}%2Fv2_lineup/".format(match_id)
        yield scrapy.Request(url, callback=self.parse_squads, meta={"match": match})

    @staticmethod
    def parse_squads(response):
        # type: (response) -> request
        """
        Fills in match squads info into object and yields request into match events crawl.

        :rtype: request
        :param response: match squads response.
        :return: match events request.
        """
        match = response.meta["match"]
        players = response.xpath('//div[@class="mx-lineup-incident-name"]/text()').extract()
        players_pos = response.xpath('//table[@class="mx-lineup-table"]')
        home_players_pos = players_pos[0].re('("mx-table-splitrow"|"mx-lineup-row")')
        away_players_pos = players_pos[2].re('("mx-table-splitrow"|"mx-lineup-row")')

        # region short crutch because of data error
        if len(home_players_pos) < 15:
            home_players_pos = [u"mx-table-splitrow", u"mx-lineup-row"] + home_players_pos
            players = [""] + players
        if len(away_players_pos) < 15:
            away_players_pos = [u"mx-table-splitrow", u"mx-lineup-row"] + away_players_pos
            players = players[:11] + [""] + players[11:]
        # endregion

        match["home_players"] = players[:11]
        match["away_players"] = players[11:]
        match["home_players_pos"] = home_players_pos
        match["away_players_pos"] = away_players_pos
        # from scrapy.exceptions import CloseSpider
        # raise CloseSpider("bandwidth_exceeded")
        # print match
        yield match
