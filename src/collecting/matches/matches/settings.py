# -*- coding: utf-8 -*-
# scrapy settings for matches project

BOT_NAME = "matches"

SPIDER_MODULES = ["matches.spiders"]
NEWSPIDER_MODULE = "matches.spiders"

LOG_LEVEL = "DEBUG"

# crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = "matches (+http://www.yourdomain.com)"

# obey robots.txt rules
ROBOTSTXT_OBEY = True

# configure maximum concurrent requests performed by scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# configure a delay for requests for the same website (default: 0)
# DOWNLOAD_DELAY = 3

# the download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# disable cookies (enabled by default)
# COOKIES_ENABLED = False

# disable telnet console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
#   "Accept-Language": "en",
# }

# enable or disable spider middlewares
# SPIDER_MIDDLEWARES = {
#    "matches.middlewares.MatchesSpiderMiddleware": 543,
# }

# enable or disable downloader middlewares
# DOWNLOADER_MIDDLEWARES = {
#    "matches.middlewares.MyCustomDownloaderMiddleware": 543,
# }

# enable or disable extensions
# EXTENSIONS = {
#    "scrapy.extensions.telnet.TelnetConsole": None,
# }

# configure item pipelines
ITEM_PIPELINES = {"matches.pipelines.JsonPipeline": 300}

# enable and configure the autothrottle extension (disabled by default)
# AUTOTHROTTLE_ENABLED = True
# the initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# the maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# the average number of requests scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# enable and configure HTTP caching (disabled by default)
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = "httpcache"
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = "scrapy.extensions.httpcache.FilesystemCacheStorage"
