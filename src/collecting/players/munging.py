import re
import config
from dateutil import parser
from collections import OrderedDict


def get_number(line):
    # type: (str) -> int
    """
    Returns integer value from a given string.

    :rtype: int
    :param line: raw string.
    :return: integer value.
    """
    if line is None:
        return -1
    return int(re.sub("[^0-9]", "", line))


def split_name_id(line):
    # type: (str) -> (str, int)
    """
    Splits name_id raw string and returns player name and sofifa id.

    :rtype: str, int
    :param line: raw string.
    :return: name and sofifa id.
    """
    line = unicode(line)
    splitted = line.split("(")
    name = splitted[0].strip()
    sf_id = filter(unicode.isdigit, splitted[1])
    return name, get_number(sf_id)


def get_full_name(line):
    # type: (str) -> str
    """
    Returns player full name from a given string.

    :rtype: str
    :param line: raw string.
    :return: full name.
    """
    return line.strip()


def get_birth_height_weight(line):
    # type: (str) -> (datetime, int, int)
    """
    Splits raw string and returns birth, height and weight entries.

    :rtype: datetime, int, int
    :param line: raw string.
    :return: birth, height and weight.
    """
    birth = line[line.rfind("(") + 1: line.rfind(")")]
    line = line.split(" ")
    return parser.parse(birth), get_number(line[5]), get_number(line[6])


def get_work_rate(line):
    # type: (str) -> (int, int)
    """
    Splits work_rate raw string and returns work rates integer entries.

    :rtype: int, int
    :param line: raw string.
    :return: work rates entries.
    """
    line = line.split("/")
    first = line[0].split()[-1]
    second = line[1]
    mapping = config.sofifa_player_wrs
    return mapping[first.strip()], mapping[second.strip()]


def get_stats(line):
    # type: (str) -> dict
    """
    Returns dictionary of stats from a given string.

    :rtype: dict
    :param line: raw string.
    :return: dict of stats.
    """
    output = OrderedDict()
    for stat in config.sofifa_player_stats:
        parsed = re.search(r"(?<=point{} = ).*?(?=;)".format(stat), line)
        if parsed is not None:
            output[stat] = int(parsed.group(0))
        else:
            output[stat] = -1
    return output


def get_version(line):
    # type: (str) -> str
    """
    Returns player version without "FIFA ##" instance.

    :rtype: str
    :param line: raw string.
    :return: player version.
    """
    return line[line.find("FIFA")+7:line.find("SoFIFA")].strip()
