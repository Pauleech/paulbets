# -*- coding: utf-8 -*-
import config
import random
import munging
import pandas as pd
import modules.timing as t
import concurrent.futures
import modules.sqlitedb as sqlite
import modules.browsing as browsing
from lxml import html
from dateutil import parser
from collections import OrderedDict
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_player_fields(tree):
    # type: (html) -> dict
    """
    Scraps player fields into dictionary for a given url tree.

    :rtype: dict
    :param tree: html.
    :return: player fields dictionary.
    """
    output = dict()
    output["version"] = tree.xpath("/html/head/title/text()")[0]
    output["name_id"] = tree.xpath("/html/body/section/section/article/div[1]/div[1]/h1")[0].text
    output["full_name"] = tree.xpath("/html/body/section/section/article/div[1]/div[1]/div/span")[0].text
    output["birth_height_weight"] = tree.xpath("/html/body/section/section/article/div[1]/div[1]/div/span/text()")[-1]
    output["rating_potential"] = [el.text for el in tree.xpath("//td/span[1]")[:2]]
    output["info"] = tree.xpath("//td[1]/ul/li/text()")
    output["work_rate"] = tree.xpath("//td[1]/ul/li[5]/span")[0].text
    output["stats"] = tree.xpath("/html/body/script[1]/text()")[0]
    output["table"] = tree.xpath("/html/body/section/section/aside/div[2]/table/tbody/tr")
    for i in [3, 4]:
        contract = tree.xpath("//tr/td[{}]/ul/li[6]/label".format(i))
        if len(contract) == 0:
            output["team_id"] = None
        else:
            output["team_id"] = tree.xpath("//tr/td[{}]/ul/li[1]/a[2]".format(i))[0].attrib["href"]
            break
    return output


def scrap_player(url):
    # type: (str) -> dict
    """
    Scraps player into dictionary for a given link (used in a pool).

    :rtype: dict
    :param url: player url.
    :return: player dictionary.
    """
    page = browsing.get(url)
    tree = html.fromstring(page.content)
    player = OrderedDict()
    fields = get_player_fields(tree)

    # set player fields
    player["version"] = parser.parse(munging.get_version(fields["version"]))
    player["name"], player["sofifa_id"] = munging.split_name_id(fields["name_id"])
    player["full_name"] = munging.get_full_name(fields["full_name"])
    player["birth"], player["height"], player["weight"] = \
        munging.get_birth_height_weight(fields["birth_height_weight"])
    player["rating"] = munging.get_number(fields["rating_potential"][0])
    player["potential"] = munging.get_number(fields["rating_potential"][1])
    player["weak_foot"] = munging.get_number(fields["info"][6])
    player["skill_moves"] = munging.get_number(fields["info"][9])
    player["work_rate_1"], player["work_rate_2"] = munging.get_work_rate(fields["work_rate"])
    player["team_id"] = munging.get_number(fields["team_id"])

    # set player positions
    for pos in config.sofifa_positions:
        player[pos] = -1
    if len(fields["table"]) != 0:
        for table_el in fields["table"]:
            pos_el = table_el.getchildren()
            ova = pos_el[1].getchildren()[0].text
            for position in pos_el[0].getchildren():
                player[position.text] = int(ova)
    else:
        player["GK"] = player["rating"]

    # set player stats
    values = munging.get_stats(fields["stats"]).values()
    for stat, value in zip(config.sofifa_player_stats_vis, values):
        player[stat] = value
    return player


def scrap_players(urls):
    # type: (list) -> list
    """
    Scraps players for a given list of urls.

    :rtype: list
    :param urls: players urls.
    :return: list of players dictionaries.
    """
    output = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=config.browsing_workers) as executor:
        future_to_url = {executor.submit(scrap_player, url): url for url in urls}
        for future in concurrent.futures.as_completed(future_to_url):
            version = future_to_url[future]
            try:
                output.append(future.result())
            except Exception as e:
                logger.warn("player {} cannot be scrapped: {}".format(version, e))
    return output


def get_versions_urls(prev_version):
    # type: (str) -> list
    """
    Returns a list of tupled versions templates urls: (href, template).
    ("?{version}", "https://sofifa.com/players/{0}?{version}&offset={1}")

    :rtype: list
    :param prev_version: last scraped version class.
    :return: list of tupled versions templates urls.
    """

    def generate_version_url(href_part):
        # type: (str) -> str
        """
        Generates a version link template for a given href part.
        "https://sofifa.com/players/{}?{version}&offset={}".

        :rtype: str
        :param href_part: href part with version.
        :return: version link template str.
        """
        output = "{}{}&offset={{1}}".format(config.sofifa_website, href_part)
        output = output.replace("?", "/{0}?")
        return output

    page = browsing.get(config.sofifa_players.format(0))
    tree = html.fromstring(page.content)
    versions_els = tree.xpath('//*[@id="version-calendar"]/div/div/div[2]/div/div/div[2]/a')
    versions = [(el.attrib["class"], el.attrib["href"]) for el in versions_els]
    versions = [(href[href.rfind("?"):], generate_version_url(href)) for version, href in versions
                if version > prev_version and version[1:3].isdigit()]
    return versions


def run_scraping(sqlite_db, prev_version, debug=False):
    # type: (sqlite3, str, bool) -> None
    """
    Scraps all the players to SQLite DB.

    :rtype: None
    :param sqlite_db: SQLite DB connection.
    :param prev_version: last scraped version class.
    :param debug: debug state.
    :return: None.
    """
    with t.elapsed_timer(logger, "getting versions list"):
        versions_urls = get_versions_urls(prev_version)
    if debug:
        sample = random.sample(versions_urls, len(versions_urls) / 10)
        versions_urls = sample + [versions_urls[-1]]
    for i, version in enumerate(reversed(versions_urls)):
        version_href, version_url = version
        with t.elapsed_timer(logger, "version {}, {:03d}/{:03d}".
                format(version_href, i + 1, len(versions_urls)), title=True):
            statuses = config.sofifa_statuses
            if version_href == config.sofifa_init_version_href or debug:
                statuses = ("",)
            for status in statuses:
                offset = 0
                with t.elapsed_timer(logger, "scrapping {} players".format(status), title=True) as elapsed:
                    while True:
                        page = browsing.get(version_url.format(status, offset))
                        tree = html.fromstring(page.content)
                        players_els = tree.xpath('//*[@id="pjax-container"]/table/tbody/tr/td[2]/div/a[2]')
                        players_urls = ["{}{}{}".format(config.sofifa_website, el.attrib["href"], version_href)
                                        for el in players_els]
                        if len(players_urls) == 0:
                            break
                        players = scrap_players(players_urls)
                        offset += len(players)
                        logger.info("{} players scrapped: {:.5f}".format(offset, elapsed()))
                        df = pd.DataFrame(players)
                        sqlite.insert_data(sqlite_db, config.players_attr_table, df)
    columns = ["version", "name", "sofifa_id"]
    sqlite.delete_duplicates(sqlite_db, config.players_attr_table, columns)


def update_players_table(sqlite_db):
    # type: (sqlite3) -> None
    """
    Updates players table in SQLite DB.

    :rtype: None
    :param sqlite_db: SQLite DB connection.
    :return: None.
    """
    columns = ["full_name", "name", "sofifa_id", "birth"]
    agg_column = "team_id"
    with t.elapsed_timer(logger, "updating players table", title=True):
        sqlite.drop_table(sqlite_db, config.players_table)
        values = sqlite.fetch_unique_rows_gcd(sqlite_db, config.players_attr_table, columns, agg_column)
        values = map(lambda x: x[0:3] + (parser.parse(x[3]),) + (x[4],), values)
        df = pd.DataFrame(values, columns=columns + [agg_column])
        sqlite.insert_data(sqlite_db, config.players_table, df)
        sqlite.delete_duplicates(sqlite_db, config.players_table, columns)
