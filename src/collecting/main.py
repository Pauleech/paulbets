import config
import argparse
import modules.timing as t
import modules.sqlitedb as sqlite
import collecting.clubs.scraping as clubs
import collecting.players.scraping as players
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def scrap(debug_mode):
    # type: (bool) -> None
    """
    Runs the script.

    :rtype: None
    :param debug_mode: debug mode.
    :return: None.
    """
    sdb = sqlite.get_connection()
    with t.elapsed_timer(logger, "scrapping clubs", title=True):
        clubs.run_scraping(sdb, prev_version=config.sofifa_prev_version_class, debug=debug_mode)
    with t.elapsed_timer(logger, "sorting clubs", title=True):
        sqlite.sort_data(sdb, config.clubs_attr_table, ["version", "sofifa_id"], ["DESC", "ASC"])
    clubs.update_clubs_table(sdb)
    with t.elapsed_timer(logger, "scrapping players", title=True):
        players.run_scraping(sdb, prev_version=config.sofifa_prev_version_class, debug=debug_mode)
        sqlite.delete_duplicates(sdb, config.players_attr_table, ["sofifa_id", "version"])
    with t.elapsed_timer(logger, "sorting players", title=True):
        sqlite.sort_data(sdb, config.players_attr_table, ["version", "sofifa_id"], ["DESC", "ASC"])
    players.update_players_table(sdb)


def main():
    """
    Module main function.
    """
    parser = argparse.ArgumentParser(description="Paulbets: data scraping script.")
    subparsers = parser.add_subparsers(help="commands")
    scrap_parser = subparsers.add_parser("scrap", help="scrap data")
    scrap_parser.set_defaults(which="scrap")
    scrap_parser.add_argument("--debug_mode", default=False, type=bool, help="debug mode scrapping", required=False)
    args = vars(parser.parse_args())
    functions = {"scrap": scrap, "pass": lambda x: x}
    func = functions[args.pop("which", None)]
    func(**args)


if __name__ == "__main__":
    main()
