# -*- coding: utf-8 -*-
import re
import random
import config
import munging
import pandas as pd
import concurrent.futures
import modules.timing as t
import modules.sqlitedb as sqlite
import modules.browsing as browsing
from lxml import html
from dateutil import parser
from collections import OrderedDict
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def scrap_club(url):
    # type: (str) -> dict
    """
    Scraps club into dictionary for a given link (used in a pool).

    :rtype: dict
    :param url: club url.
    :return: club dictionary.
    """
    page = browsing.get(url)
    tree = html.fromstring(page.content)
    club = OrderedDict()
    # scrap fields
    version = tree.xpath("/html/head/title/text()")[0]
    name_id = tree.xpath("/html/body/section/section/article/div[1]/div[1]/h1")[0].text
    league = tree.xpath("/html/body/section/section/article/div[1]/div[1]/div/span/a/text()")[0]
    stats = tree.xpath("/html/body/section/section/aside/div[2]/dl/dd/span/text()")
    # set club fields
    club["version"] = parser.parse(munging.get_version(version))
    club["name"], club["sofifa_id"] = munging.split_name_id(name_id)
    club["league"] = re.sub("[^A-Za-z0-9 ]", "", league[:-3].replace(".", "")).strip()
    for stat, value in zip(config.sofifa_club_stats, stats):
        if value[0].isdigit() and stat != "b_dri":
            club[stat] = munging.get_number(value)
        else:
            club[stat] = munging.get_text(value).lower()
    return club


def scrap_clubs(urls):
    # type: (list) -> list
    """
    Scraps clubs for a given list of urls.

    :rtype: list
    :param urls: clubs urls.
    :return: list of clubs dictionaries.
    """
    output = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=config.browsing_workers) as executor:
        future_to_url = {executor.submit(scrap_club, url): url for url in urls}
        for future in concurrent.futures.as_completed(future_to_url):
            version = future_to_url[future]
            try:
                output.append(future.result())
            except Exception as e:
                logger.warn("club {} cannot be scrapped: {}".format(version, e))
    return output


def get_versions_urls(prev_version):
    # type: (str) -> list
    """
    Returns a list of tupled versions templates urls: (href, template).
    ("?{version}", "https://sofifa.com/teams/club?{version}&offset={}")

    :rtype: list
    :param prev_version: last scraped version class.
    :return: list of tupled versions templates urls.
    """

    def generate_version_url(href_part):
        # type: (str) -> str
        """
        Generates a version link template for a given href part.
        "https://sofifa.com/teams/club?{version}&offset={}".

        :rtype: str
        :param href_part: href part with version.
        :return: version link template str.
        """
        output = "{}{}&offset={{0}}".format(config.sofifa_website, href_part)
        return output

    page = browsing.get(config.sofifa_clubs.format(0))
    tree = html.fromstring(page.content)
    versions_els = tree.xpath('//*[@id="version-calendar"]/div/div/div[2]/div/div/div[2]/a')
    versions = [(el.attrib["class"], el.attrib["href"]) for el in versions_els]
    versions = [(href[href.rfind("?"):], generate_version_url(href)) for version, href in versions
                if version > prev_version and version[1:3].isdigit()]
    return versions


def run_scraping(sqlite_db, prev_version, debug=False):
    # type: (sqlite3, str, bool) -> None
    """
    Scraps all the clubs to SQLite DB.

    :rtype: None
    :param sqlite_db: SQLite DB connection.
    :param prev_version: last scraped version class.
    :param debug: debug state.
    :return: None.
    """
    with t.elapsed_timer(logger, "getting versions list"):
        versions_urls = get_versions_urls(prev_version)
    if debug:
        sample = random.sample(versions_urls, len(versions_urls) / 10)
        versions_urls = sample + [versions_urls[-1]]
    for i, version in enumerate(reversed(versions_urls)):
        version_href, version_url = version
        with t.elapsed_timer(logger, "version {}, {:03d}/{:03d}".
                format(version_href, i + 1, len(versions_urls)), title=True) as elapsed:
            offset = 0
            while True:
                page = browsing.get(version_url.format(offset))
                tree = html.fromstring(page.content)
                clubs_els = tree.xpath('//*[@id="pjax-container"]/table/tbody/tr/td[2]/div/a')
                clubs_urls = ["{}{}{}".format(config.sofifa_website, el.attrib["href"], version_href)
                              for el in clubs_els]
                if len(clubs_urls) == 0:
                    break
                clubs = scrap_clubs(clubs_urls)
                offset += len(clubs)
                logger.info("{} clubs scrapped: {:.5f}".format(offset, elapsed()))

                df = pd.DataFrame(clubs)
                sqlite.insert_data(sqlite_db, config.clubs_attr_table, df)
    columns = ["version", "name", "sofifa_id", "league"]
    sqlite.delete_duplicates(sqlite_db, config.clubs_attr_table, columns)


def update_clubs_table(sqlite_db):
    # type: (sqlite3) -> None
    """
    Updates clubs table in SQLite DB.

    :rtype: None
    :param sqlite_db: SQLite DB connection.
    :return: None.
    """
    columns = ["name", "sofifa_id", "league"]
    with t.elapsed_timer(logger, "updating clubs table", title=True):
        sqlite.drop_table(sqlite_db, config.clubs_table)
        values = sqlite.fetch_unique_rows(sqlite_db, config.clubs_attr_table, columns)
        df = pd.DataFrame(values, columns=columns)
        sqlite.insert_data(sqlite_db, config.clubs_table, df)
        sqlite.delete_duplicates(sqlite_db, config.clubs_table, columns)
