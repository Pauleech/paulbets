import re


def split_name_id(line):
    # type: (str) -> (str, int)
    """
    Splits name_id raw string and returns club name and sofifa id.

    :rtype: str, int
    :param line: raw string.
    :return: name and sofifa id.
    """
    line = unicode(line)
    name = line[:line.rfind("(")].strip()
    sf_id = filter(unicode.isdigit, line[line.rfind("("):line.rfind(")")])
    return name, int(sf_id)


def get_number(line):
    # type: (str) -> int
    """
    Returns integer value from a given string.

    :rtype: int
    :param line: raw string.
    :return: integer value.
    """
    return int(re.sub("[^0-9]", "", line))


def get_text(line):
    # type: (str) -> str
    """
    Returns only letters from a given string.

    :rtype: str
    :param line: raw string.
    :return: str.
    """
    return re.sub("[^A-Za-z ]", "", line).strip()


def get_version(line):
    # type: (str) -> str
    """
    Returns club version without "FIFA ##" instance.

    :rtype: str
    :param line: raw string.
    :return: club version.
    """
    return line[line.find("FIFA")+7:line.find("SoFIFA")].strip()
