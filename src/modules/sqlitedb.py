import config
import sqlite3
import pandas as pd


def get_connection():
    # type: () -> sqlite3
    """
    Returns SQLite DB connection.

    :rtype: sqlite3
    :return: SQLite DB connection.
    """
    output = sqlite3.connect(config.db_name)
    return output


def drop_table(db, table):
    # type: (sqlite3, str) -> None
    """
    Drops table for a given naming from SQLite DB.

    :rtype: None
    :param db: SQLite DB connection.
    :param table: table name.
    :return: None.
    """
    try:
        cursor = db.cursor()
        cursor.execute("DROP TABLE {}".format(table))
        db.commit()
    except sqlite3.OperationalError:
        pass


def get_cursor(db, table):
    # type: (sqlite3, str) -> Cursor
    """
    Fetches a Cursor for full SQLite DB iterating.

    :rtype: Cursor
    :param db: SQLite DB connection.
    :param table: table name.
    :return: Cursor for full SQLite DB iterating.
    """
    cursor = db.cursor()
    cursor.execute("SELECT * FROM {}".format(table))
    return cursor


def insert_data(db, table, df):
    # type: (sqlite3, str, DataFrame) -> None
    """
    Appends data into SQLite DB table (creates table if not exist).

    :rtype: None
    :param db: SQLite DB connection.
    :param table: table name.
    :param df: players DataFrame.
    :return: None.
    """
    df.to_sql(table, db, if_exists="append", index=False)


def read_data(db, table):
    # type: (sqlite3, str) -> DataFrame
    """
    Reads DataFrame data from SQLite DB table.

    :rtype: DataFrame
    :param db: SQLite DB connection.
    :param table: table name.
    :return: data.
    """
    return pd.read_sql_query("SELECT * FROM {}".format(table), db)


def delete_duplicates(db, table, columns):
    # type: (sqlite3, str, list) -> None
    """
    Delete duplicates for given table by given columns.

    :rtype: None
    :param db: SQLite DB connection.
    :param table: table name.
    :param columns: columns list.
    :return: None.
    """
    cursor = db.cursor()
    cursor.execute("DELETE FROM {0} "
                   "WHERE rowid NOT IN "
                   "("
                   "SELECT MIN(rowid)"
                   "FROM {0} "
                   "GROUP BY {1}"
                   ")".format(table, ", ".join(columns)))
    db.commit()


def sort_data(db, table, columns, sort_directions=None):
    # type: (sqlite3, str, list, list) -> None
    """
    Sorts data for given table and by given columns.

    :rtype: None
    :param db: SQLite DB connection.
    :param table: table name.
    :param columns: columns list for sorting.
    :param sort_directions: sorting directions for each of the columns
    :return: None.
    """
    if sort_directions is None:
        sort_directions = ["ASC"] * len(columns)
    columns = ["{} {}".format(c, d) for c, d in zip(columns, sort_directions)]
    cursor = db.cursor()
    cursor.execute("CREATE TABLE temp "
                   "AS SELECT * FROM {} WHERE 0".format(table))
    cursor.execute("INSERT INTO temp "
                   "SELECT * FROM {} "
                   "ORDER BY {}".format(table, ", ".join(columns)))
    cursor.execute("DROP TABLE {}".format(table))
    cursor.execute("ALTER TABLE temp RENAME TO {}".format(table))
    db.commit()


def fetch_unique_rows(db, table, columns):
    # type: (sqlite3, str, list) -> list
    """
    Returns unique values list for a given columns.

    :rtype: list
    :param db: SQLite DB connection.
    :param table: table name.
    :param columns: column names list.
    :return: unique values list for a given columns.
    """
    cursor = db.cursor()
    query = "SELECT DISTINCT {} FROM {}".format(", ".join(columns), table)
    cursor.execute(query)
    return cursor.fetchall()


def fetch_unique_rows_gcd(db, table, columns, agg_column):
    # type: (sqlite3, str, list) -> list
    """
    Returns unique values list with grouping by columns and aggregating distinct agg_column concatenation.

    :rtype: list
    :param db: SQLite DB connection.
    :param table: table name.
    :param columns: column names list.
    :param agg_column: aggregation column.
    :return: unique values list for a given columns.
    """
    cursor = db.cursor()
    cursor.execute("SELECT {1}, GROUP_CONCAT(DISTINCT {2}) "
                   "FROM {0} "
                   "GROUP BY {1} ".format(table, ", ".join(columns), agg_column))
    return cursor.fetchall()
