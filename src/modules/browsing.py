import time
import config
import requests
from proxy import Proxy
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)
proxies = Proxy().proxy_list


def get(url):
    # type: (str) -> requests.Response
    """
    Performs requests.get with a rate limitation.

    :rtype: requests.Response
    :param url: web-url.
    :return: requests.Response.
    """
    result = None
    for i in range(config.browsing_retrying):
        try:
            time.sleep(config.browsing_delay)
            result = requests.get(url, timeout=5, proxies={"http": proxies[0]})
            break
        except requests.exceptions.Timeout:
            result = None
            logger.warn("retrying.. timeout for {}".format(url))
        except requests.exceptions.ConnectionError:
            result = None
            logger.warn("retrying.. connection error for {}".format(url))
            del proxies[0]
    if result is None:
        raise requests.exceptions.Timeout
    return result
