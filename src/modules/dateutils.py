import numpy as np
from datetime import datetime


def get_datestr():
    # type: () -> str
    """
    Returns current datetime as string.

    :rtype: str
    :return: datetime string.
    """
    return datetime.strftime(datetime.now(), "%Y-%m-%d_%H-%M-%S")


def get_timedelta_days(value):
    # type: (value) -> int
    """
    Returns an exact number of days for a given timedelta.

    :rtype: int
    :param value: timedelta.
    :return: number of days.
    """
    x = np.timedelta64(value, "ns")
    days = x.astype("timedelta64[D]")
    return int(days / np.timedelta64(1, "D"))
