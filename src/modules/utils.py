import heapq
from operator import itemgetter
from itertools import compress, product


def unique(values):
    # type: (list) -> list
    """
    Returns list with unique values for a given list.

    :rtype: list
    :param values: list.
    :return: list with unique values.
    """

    def _f(v):
        seen = set()
        for x in v:
            if x not in seen:
                seen.add(x)
                yield x

    return list(_f(values))


def get_nlt_max(values, pos_max, pos_ret, nlargest=1, threshold=-float("inf")):
    # type: (list, int, int, int, float) -> object
    """
    Defines nlargest "maximal" tuple in a list of tuples regarding its element as pos_max position.
    Returns element of this tuple at pos_ret position.

    :rtype: object
    :param values: a list.
    :param pos_max: element position in tuple to define "maximal" tuple.
    :param pos_ret: element position in "maximal" tuple to return value.
    :param nlargest: defines number of largest (first max, second max, ).
    :param threshold: returns None if pos_max element in nlargest "maximal" tuple is < threshold.
    :return: element of nlargest "maximal" tuple at pos_ret position.
    """
    res = heapq.nlargest(nlargest, values, key=itemgetter(pos_max))[nlargest - 1]
    if res[pos_max] < threshold:
        return None
    else:
        return res[pos_ret]


def get_combinations(values):
    # type: (list) -> list
    """
    Returns list of all possible combinations for a given list.

    :rtype: list
    :param values: list.
    :return: list with combinations.
    """
    return list(list(set(compress(values, mask))) for mask in product(*[[0, 1]] * len(values)))[1:]


if __name__ == "__main__":
    xx = [(0, 1), (0.4, 4), (0.5, 5), (0.6, 6), (0.7, 7)]
    print get_nlt_max(xx, pos_max=0, pos_ret=1, nlargest=2, threshold=0.6)
