import requests
from lxml import html


class Proxy:
    proxy_url = "https://www.ip-adress.com/proxy-list"
    proxy_list = []

    def __init__(self):
        # type: () -> None
        """
        Initiates Proxy object.

        :rtype: None
        :return: None.
        """
        page = requests.get(self.proxy_url)
        tree = html.fromstring(page.content)
        proxies_ip = tree.xpath("//tr/td[1]/a/text()")
        proxies_ports = tree.xpath("//tr/td[1]/text()")
        proxies_types = tree.xpath("//tr/td[2]/text()")
        self.proxy_list = ["http://{}{}".format(ip, port)
                           for ip, port, ip_type in zip(proxies_ip, proxies_ports, proxies_types)
                           if ip_type == "highly-anonymous"]


if __name__ == "__main__":
    print Proxy().proxy_list
