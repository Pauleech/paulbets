data {
  int<lower=1> nteams;
  int<lower=1> ngames;
  int<lower=1> nweeks;
  int<lower=1> home_week[ngames];
  int<lower=1> away_week[ngames];
  int<lower=1, upper=nteams> home_team[ngames];
  int<lower=1, upper=nteams> away_team[ngames];
  vector[ngames] score_diff;
  row_vector[nteams] prev_perf;
}

transformed data {
}

parameters {
  real prev;
  real<lower=0> sigma_a0;
  real<lower=0> tau_a;
  real<lower=1> nu;
  real<lower=0> sigma_y;

  row_vector<lower=0>[nteams] sigma_a_raw;
  matrix[nweeks,nteams] eta_a;
}

transformed parameters {
  matrix[nweeks, nteams] a;
  row_vector<lower=0>[nteams] sigma_a;

  a[1] = prev * prev_perf + sigma_a0 * eta_a[1];
  sigma_a = tau_a * sigma_a_raw;
  for (w in 2:nweeks) {
    a[w] = a[w-1] + sigma_a .* eta_a[w];
  }
}

model {
  vector[ngames] a_diff;

  nu ~ gamma(2,0.1);
  prev ~ normal(0,1);
  sigma_a0 ~ normal(0,1);
  sigma_y ~ normal(0,5);
  sigma_a_raw ~ normal(0,1);
  tau_a ~ cauchy(0,1);
  to_vector(eta_a) ~ normal(0,1);

  for (g in 1:ngames) {
     a_diff[g] = a[home_week[g],home_team[g]] - a[away_week[g],away_team[g]];
  }
  score_diff ~ student_t(nu, a_diff, sigma_y);
}