import os
import pickle
import config
import numpy as np
import pandas as pd
from pystan import StanModel
from collections import defaultdict
from modules.logering import setup_logger
from modules.suppress import suppress_stdout_stderr
logger = setup_logger(__name__, config.log_name, config.log_level)


class League:
    def __init__(self, data, model=None):
        # type: (DataFrame, object) -> None
        """
        Initiates League object.

        :rtype: None
        :param data: matches DataFrame.
        :return: None.
        """
        # main dataframes
        self.results = data
        self.teams = None
        # class lists
        self.home_team = None
        self.away_team = None
        self.home_game = None
        self.away_game = None
        self.observed_diff = None
        self.prev_perf = None
        # class variables
        self.num_games = 0
        self.num_teams = 0
        self.num_weeks = 0
        self.team_games = 0
        # model
        self.periods = None
        self.sm = model

    def set_teams(self):
        # type: () -> None
        """
        Sets teams DataFrame with names and indices.

        :rtype: None
        :return: None.
        """
        teams = sorted(np.unique(list(self.results["home_team"]) + list(self.results["away_team"])))
        teams = pd.DataFrame(teams, columns=["team"])
        teams["i"] = teams.index
        self.teams = teams

    def preprocess_results(self):
        # type: () -> None
        """
        Processes season results and sets class variables for model training.

        :rtype: None
        :return: None.
        """
        self.results = pd.merge(self.results, self.teams, left_on="home_team", right_on="team", how="left")
        self.results = self.results.rename(columns={"i": "i_home"}).drop("team", 1)
        self.results = pd.merge(self.results, self.teams, left_on="away_team", right_on="team", how="left")
        self.results = self.results.rename(columns={"i": "i_away"}).drop("team", 1)
        # region variables set
        self.num_games = len(self.results)
        self.observed_diff = self.results["home_goal"].values - self.results["away_goal"].values
        self.home_team = self.results["i_home"].values
        self.away_team = self.results["i_away"].values
        self.num_teams = len(self.teams)
        self.team_games = self.num_teams * 2 - 2
        self.num_weeks = self.num_games / (self.num_teams / 2)
        self.prev_perf = np.array([0.5] * self.num_teams)
        # endregion
        freq_dict = defaultdict(int)
        game_home = np.zeros(self.num_games)
        game_away = np.zeros(self.num_games)
        for i in range(self.num_games):
            home, away = self.results["home_team"].iloc[i], self.results["away_team"].iloc[i]
            freq_dict[home] += 1
            freq_dict[away] += 1
            game_home[i] = freq_dict[home] - 1
            game_away[i] = freq_dict[away] - 1
        self.results["home_game"] = pd.Series(game_home.astype(int), index=self.results.index)
        self.results["away_game"] = pd.Series(game_away.astype(int), index=self.results.index)
        self.home_game = self.results["home_game"].values
        self.away_game = self.results["away_game"].values

    def set_stages(self):
        # type: () -> None
        """
        Sets stages.

        :rtype: None
        :return: None.
        """
        output = []
        teams = self.results["home_team"].values
        stages_dict = defaultdict(int)
        for team in teams:
            stages_dict[team] += 1
            output.append(stages_dict[team])
        self.results["stage"] = pd.Series(output)

    def set_periods(self):
        # type: () -> None
        """
        Sets periods regarding playing rounds (stages).

        :rtype: None
        :return: None.
        """
        rounds = sorted(self.results["stage"].values)
        cur = rounds[0]
        self.periods = [[cur], ]
        for i in range(1, len(rounds)):
            if rounds[i] == cur:
                self.periods[-1].append(rounds[i])
            else:
                cur = rounds[i]
                self.periods.append([cur])
        self.periods = [len(v) for v in self.periods]
        self.periods = np.cumsum(self.periods)

    def prepare_data(self):
        # type: () -> None
        """
        Prepares season results data for training.

        :rtype: None
        :return: None.
        """
        self.set_teams()
        self.preprocess_results()
        self.set_stages()
        self.set_periods()

    @staticmethod
    def get_model():
        # type: () -> object
        """
        Compiles or loads compiled Stan model.

        :rtype: object.
        :return: Stan model.
        """
        filename = os.path.join(config.bayesball_path, "student.pickle")
        if os.path.isfile(filename):
            sm = pickle.load(open(filename, "rb"))
        else:
            sm = StanModel(os.path.join(config.bayesball_path, "student.stan"), verbose=False)
            with open(os.path.join(config.bayesball_path, "student.pickle"), "wb") as f:
                pickle.dump(sm, f)
        return sm

    def train_model(self, forms, cur_indices):
        # type: (dict, tuple) -> dict
        """
        Trains Stan model and updates season forms dict.

        :rtype: dict
        :param forms: season forms dict.
        :param cur_indices: current matches indices.
        :return: updated season forms dict.
        """
        samples = 750
        bayes_dat = {"nteams": self.num_teams,
                     "ngames": self.num_games,
                     "nweeks": max(np.concatenate((self.home_game + 1, self.away_game + 1))),
                     "home_team": self.home_team + 1,
                     "away_team": self.away_team + 1,
                     "home_week": self.home_game + 1,
                     "away_week": self.away_game + 1,
                     "prev_perf": self.prev_perf[:],
                     "score_diff": self.observed_diff
                     }
        with suppress_stdout_stderr():
            fit = self.sm.sampling(data=bayes_dat, iter=samples, verbose=False)
            trace = fit.extract(permuted=True)
        for g in range(*cur_indices):
            match = self.results.loc[g]
            if match["home_team"] not in forms:
                forms[match["home_team"]] = []
            if match["away_team"] not in forms:
                forms[match["away_team"]] = []
            forms[match["home_team"]].append(np.median(trace["a"][:, self.home_game[g], self.home_team[g]], axis=0))
            forms[match["away_team"]].append(np.median(trace["a"][:, self.away_game[g], self.away_team[g]], axis=0))
        return forms
