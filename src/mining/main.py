import config
import features
import extraction
import numpy as np
import pandas as pd
import bayesball.league as bayes
import modules.sqlitedb as sqlite
from modules import utils
from modules import dateutils
from dateutil import parser
from collections import defaultdict
from modules.timing import elapsed_timer, timeit
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)
pd.options.mode.chained_assignment = None


class FeaturesExtractor(object):
    def __init__(self):
        # type: () -> None
        """
        Initiates FeaturesExtractor object.

        :rtype: None
        :return: None.
        """
        self.sdb = sqlite.get_connection()
        self.bayes = bayes.League.get_model()
        self.matches = None
        # sofifa data
        self.clubs_data = None
        self.players_data = None
        # calculable data
        self.data = None
        self.standings = dict()
        self.forms = dict()
        self.events = dict()
        self.freshness = dict()

    @timeit(logger, "loading matches", title=True)
    def load_matches(self):
        # type: () -> None
        """
        Loads matches from .csv files.

        :rtype: None
        :return: None.
        """
        self.matches = extraction.get_matches_data()
        self.matches = self.matches.sort_values(["date"])
        self.matches["sdate"] = self.matches["date"].apply(lambda x: x.strftime("%Y-%m-%d"))

    @timeit(logger, "loading clubs", title=True)
    def load_clubs(self):
        # type: () -> None
        """
        Loads sofifa clubs.

        :rtype: None
        :return: None.
        """
        self.clubs_data = sqlite.read_data(self.sdb, config.clubs_attr_table)
        self.clubs_data["version"] = self.clubs_data["version"].apply(lambda x: parser.parse(x))

    @timeit(logger, "loading players", title=True)
    def load_players(self):
        # type: () -> None
        """
        Loads sofifa players.

        :rtype: None
        :return: None.
        """
        self.players_data = sqlite.read_data(self.sdb, config.players_attr_table)
        self.players_data["version"] = self.players_data["version"].apply(lambda x: parser.parse(x))

    def update_standings(self, cur_df):
        # type: (DataFrame) -> None
        """
        Updates standings with a given data.

        :rtype: None
        :param cur_df: current matches data..
        :return: None.
        """
        seasons = utils.unique(cur_df["season"].values)
        leagues = utils.unique(cur_df["league"].values)
        for season in seasons:
            for league in leagues:
                mask = ((self.data["league"] == league) & (self.data["season"] == season))
                self.standings[season, league] = features.create_season_table(self.data[mask])

    def update_forms(self):
        # type: () -> None
        """
        Updates forms.

        :rtype: None
        :return: None.
        """
        date = max(self.data["sdate"])
        date_mask = (self.data["sdate"] == date)
        seasons = utils.unique(self.data.loc[date_mask]["season"].values)
        leagues = utils.unique(self.data.loc[date_mask]["league"].values)
        cols = ["home_team", "away_team", "home_goal", "away_goal", "season", "league", "date"]
        for season in seasons:
            for league in leagues:
                if (season, league) not in self.forms:
                    self.forms[season, league] = dict()
                mask = ((self.data["league"] == league) & (self.data["season"] == season))
                cur_indices = len(self.data.loc[mask]) - len(self.data.loc[mask & date_mask]), len(self.data.loc[mask])
                bayesball = bayes.League(self.data.loc[mask][cols], model=self.bayes)
                bayesball.prepare_data()
                self.forms[season, league] = bayesball.train_model(self.forms[season, league], cur_indices)

    @staticmethod
    def update_team_events(data, events, team):
        # type: (DataFrame, dict, str) -> dict
        """
        Returns updated events for a given matches data and team.

        :rtype: dict
        :param data: matches data.
        :param events: season events.
        :param team: team name.
        :return: updated events.
        """
        for active in [True, False]:
            guid = "_a{}".format(config.mining_slide) if active else "_p{}".format(config.mining_slide)
            # region lambda functions
            fuse_event_a = lambda x: x[home_event] if x["home_team"] == team else x[away_event]
            fuse_event_p = lambda x: x[away_event] if x["home_team"] == team else x[home_event]
            fuse_event = lambda x: fuse_event_a(x) if active else fuse_event_p(x)
            # endregion
            w = config.mining_slide
            # ["goal", "form", "win", "draw", "loss"]
            if team not in events:
                events[team] = dict()
            for event in config.mining_to_means:
                home_event = "home_{}".format(event)
                away_event = "away_{}".format(event)
                values = data.apply(fuse_event, axis=1).values
                mean_event = values[-w:].mean() if len(values) >= w else np.nan
                if event + guid not in events[team]:
                    events[team][event + guid] = []
                events[team][event + guid].append(mean_event)
        return events

    def update_events(self):
        # type: () -> None
        """
        Updates events.

        :rtype: None
        :return: None.
        """
        date = max(self.data["sdate"])
        date_mask = (self.data["sdate"] == date)
        seasons = utils.unique(self.data.loc[date_mask]["season"].values)
        leagues = utils.unique(self.data.loc[date_mask]["league"].values)
        for season in seasons:
            for league in leagues:
                if (season, league) not in self.events:
                    self.events[season, league] = dict()
                mask = \
                    ((self.data["league"] == league) & (self.data["season"] == season) & (self.data["sdate"] == date))
                teams = \
                    utils.unique(list(self.data[mask]["home_team"].values) + list(self.data[mask]["away_team"].values))
                for team in teams:
                    mask = ((self.data["league"] == league) & (self.data["season"] == season) & (
                    (self.data["home_team"] == team) | (self.data["away_team"] == team)))
                    self.events[season, league] = \
                        self.update_team_events(self.data[mask], self.events[season, league], team)

    @staticmethod
    def update_team_freshness(prev_df, cur_df, freshness, team):
        """
        Return updated freshness for a given prev matches data, cur matches data and team.

        :rtype: dict
        :param prev_df: previous matches data.
        :param cur_df: current matches data.
        :param freshness: season freshness.
        :param team: team name.
        :return: updated freshness.
        """
        if team not in freshness:
            freshness[team] = dict()
        event = "fresh"
        values = prev_df["date"].values
        value = dateutils.get_timedelta_days(cur_df["date"].values[0] - values[-1]) if len(values) > 0 else np.nan
        if event not in freshness[team]:
            freshness[team][event] = []
        freshness[team][event].append(value)
        return freshness

    def update_freshness(self, cur_df):
        # type: () -> None
        """
        Updates freshness.

        :rtype: None
        :param cur_df: current matches data.
        :return: None.
        """
        seasons = utils.unique(cur_df["season"].values)
        leagues = utils.unique(cur_df["league"].values)
        for season in seasons:
            for league in leagues:
                if (season, league) not in self.freshness:
                    self.freshness[season, league] = dict()
                mask = ((cur_df["league"] == league) & (cur_df["season"] == season))
                teams = utils.unique(list(cur_df[mask]["home_team"].values) + list(cur_df[mask]["away_team"].values))
                for team in teams:
                    mask = ((cur_df["league"] == league) & (cur_df["season"] == season) & (
                    (cur_df["home_team"] == team) | (cur_df["away_team"] == team)))
                    mask_ = ((self.data["league"] == league) & (self.data["season"] == season) & (
                    (self.data["home_team"] == team) | (self.data["away_team"] == team)))
                    self.freshness[season, league] = \
                        self.update_team_freshness(self.data[mask_], cur_df[mask], self.freshness[season, league], team)

    def update_data(self, cur_df):
        # type: (DataFrame) -> None
        """
        Updates data.

        :rtype: None
        :param cur_df: current matches data.
        :return: None.
        """
        self.update_standings(cur_df)
        self.update_forms()
        self.update_events()
        self.update_freshness(cur_df)

    def append_standings(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends standings to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """
        seasons = utils.unique(cur_df["season"].values)
        leagues = utils.unique(cur_df["league"].values)
        home_positions = np.zeros(len(cur_df))
        away_positions = np.zeros(len(cur_df))
        home_points = np.zeros(len(cur_df))
        away_points = np.zeros(len(cur_df))
        for season in seasons:
            for league in leagues:
                mask = ((cur_df["league"] == league) & (cur_df["season"] == season))
                home = pd.merge(cur_df[mask], self.standings[season, league], how="left",
                                left_on="home_team_id", right_on="team_id")["position"]
                home_positions[mask.values] = home.values
                away = pd.merge(cur_df[mask], self.standings[season, league], how="left",
                                left_on="away_team_id", right_on="team_id")["position"]
                away_positions[mask.values] = away.values
                home = pd.merge(cur_df[mask], self.standings[season, league], how="left",
                                left_on="home_team_id", right_on="team_id")["points"]
                home_points[mask.values] = home.values
                away = pd.merge(cur_df[mask], self.standings[season, league], how="left",
                                left_on="away_team_id", right_on="team_id")["points"]
                away_points[mask.values] = away.values
        cur_df["home_position"] = pd.Series(home_positions)
        cur_df["away_position"] = pd.Series(away_positions)
        cur_df["diff_points"] = pd.Series(home_points) - pd.Series(away_points)
        return cur_df

    def append_forms(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends forms to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """

        def get_form(forms, season, league, team):
            try:
                form = forms[(season, league)][team][-1]
                return form
            except Exception:
                return np.nan

        cur_df["home_form"] = \
            cur_df.apply(lambda x: get_form(self.forms, x["season"], x["league"], x["home_team"]), axis=1)
        cur_df["away_form"] = \
            cur_df.apply(lambda x: get_form(self.forms, x["season"], x["league"], x["away_team"]), axis=1)
        return cur_df

    def append_events(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends events to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """

        def get_event(events, season, league, team, event):
            try:
                event = events[(season, league)][team][event][-1]
                return event
            except Exception:
                return np.nan

        for active in [True, False]:
            guid = "_a{}".format(config.mining_slide) if active else "_p{}".format(config.mining_slide)
            for event_name in config.mining_to_means:
                ev = event_name + guid
                cur_df["home_" + ev] = cur_df.apply(
                    lambda x: get_event(self.events, x["season"], x["league"], x["home_team"], ev), axis=1)
                cur_df["away_" + ev] = cur_df.apply(
                    lambda x: get_event(self.events, x["season"], x["league"], x["away_team"], ev), axis=1)
        return cur_df

    def append_freshness(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends freshness to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """

        def get_event(events, season, league, team, event):
            try:
                event = events[(season, league)][team][event][-1]
                return event
            except Exception:
                return np.nan

        cur_df["home_fresh"] = \
            cur_df.apply(lambda x: get_event(self.freshness, x["season"], x["league"], x["home_team"], "fresh"), axis=1)
        cur_df["away_fresh"] = \
            cur_df.apply(lambda x: get_event(self.freshness, x["season"], x["league"], x["away_team"], "fresh"), axis=1)
        return cur_df

    def append_clubs_attr(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends sofifa clubs attributes to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """
        attr_dict = defaultdict(list)
        for index, row in cur_df.iterrows():
            for field in ["home", "away"]:
                club_id = row["{}_team_id".format(field)]
                club_versions = self.clubs_data[(self.clubs_data["sofifa_id"] == club_id) &
                                                (self.clubs_data["version"] < row["date"])]
                if len(club_versions != 0):
                    club_current = club_versions.iloc[0]
                else:
                    club_current = None
                for stat in config.sofifa_club_stats:
                    value = club_current[stat] if club_current is not None else np.nan
                    attr_dict["{}_{}".format(field, stat)].append(value)
        attr_data = pd.DataFrame.from_dict(attr_dict)
        return pd.concat([cur_df, attr_data], axis=1)

    def append_players_attr(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Appends sofifa players attributes to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """
        attr_dict = defaultdict(list)
        for index, row in cur_df.iterrows():
            for field in config.home_away:
                players = features.get_club_players(self.players_data, row["{}_team_id".format(field)], row["date"])
                players["position"] = players[config.sofifa_positions].idxmax(axis=1)
                players["position"] = players["position"].apply(lambda x: config.sofifa_position_categories[x])
                g = players.groupby("position")["rating", "potential"].quantile(.75)
                for i, stat in enumerate(["att", "def", "gk", "mid"]):
                    try:
                        attr_dict["{}_rat_{}".format(field, stat)].append(g.values[i][0])
                        attr_dict["{}_pot_{}".format(field, stat)].append(g.values[i][1])
                    except IndexError:
                        attr_dict["{}_rat_{}".format(field, stat)].append(np.nan)
                        attr_dict["{}_pot_{}".format(field, stat)].append(np.nan)
        attr_data = pd.DataFrame.from_dict(attr_dict)
        return pd.concat([cur_df, attr_data], axis=1)

    def append_features(self, cur_df):
        # type: (DataFrame) -> DataFrame
        """
        Calculates or appends features to the given current matches DataFrame.

        :rtype: DataFrame
        :param cur_df: current matches data.
        :return: updated current matches DataFrame.
        """
        cur_df = self.append_standings(cur_df)
        cur_df = self.append_forms(cur_df)
        cur_df = self.append_events(cur_df)
        cur_df = self.append_freshness(cur_df)
        cur_df = self.append_clubs_attr(cur_df)
        cur_df = self.append_players_attr(cur_df)
        cur_df = features.calculate_composite_features(cur_df)
        return cur_df

    def run_mining(self):
        # type: () -> None
        """
        Launches data mining.

        :rtype: None
        :return: None.
        """
        self.load_matches()
        self.load_clubs()
        self.load_players()
        sqlite.drop_table(self.sdb, config.matches_table)
        dates = np.unique(self.matches["sdate"])
        n = len(dates)
        for i, date in enumerate(dates):
            with elapsed_timer(logger, "day {} of {}".format(i + 1, n)):
                cur_df = self.matches.loc[self.matches["sdate"] == date].reset_index()
                cur_df = extraction.match_clubs(self.sdb, cur_df)

                if self.data is None:
                    self.data = features.calculate_results(cur_df)
                    self.update_forms()
                    self.data = self.append_forms(self.data)
                    continue

                self.update_data(cur_df)
                cur_df = self.append_features(cur_df)
                self.data = self.data.append(features.calculate_results(cur_df))
                cur_df = features.calculate_results(cur_df)
                sqlite.insert_data(self.sdb, config.matches_table, cur_df)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    model = FeaturesExtractor()
    model.run_mining()


if __name__ == "__main__":
    main()
