import os
import glob
import config
import pandas as pd
import modules.utils as lists
import modules.sqlitedb as sqlite
from dateutil import parser
from difflib import SequenceMatcher
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_matches_data():
    # type: () -> DataFrame
    """
    Returns matches DataFrame.

    :rtype: DataFrame
    :return: matches DataFrame.
    """
    dfs = []
    for filename in glob.glob(os.path.join(config.matches_folder, "*.csv")):
        guid = filename[filename.rfind("/")+1:].replace(".csv", "")
        league = " ".join(guid.split()[:-1]).strip()
        season = guid.split()[-1]
        df = pd.read_csv(filename)
        df["league"], df["season"] = league, season
        df["date"] = df["Date"].apply(lambda x: parser.parse(x, dayfirst=True))
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    df.rename(columns=config.matches_rename, inplace=True)
    return df[config.matches_columns]


def get_similarity(line, ref_lines, split_ref=False, split_source=False):
    # type: (str, list, bool, bool) -> float
    """
    Returns average similarity for given line and list of reference strings.

    :rtype: float
    :param line: a string.
    :param ref_lines: list of reference strings.
    :param split_ref: split reference lines.
    :param split_source: split source lines.
    :return: similarity value.
    """
    # short crutch for few clubs
    if line in config.clubs_conv:
        line = config.clubs_conv[line]
    output = []
    lines = [" ".join(comb) for comb in lists.get_combinations(line.split())] if split_source else [line]
    if split_ref:
        for crop_line in lines:
            for ref_line in ref_lines:
                ref_splits = lists.get_combinations(ref_line.replace(".", "").split())
                ref_combo_lines = [" ".join(comb) for comb in ref_splits]
                output.append(max([SequenceMatcher(None, crop_line, ref_combo_line).ratio()
                                   for ref_combo_line in ref_combo_lines]))
        return sum(output) / float(len(ref_lines) * len(lines))
    else:
        for crop_line in lines:
            output.append(sum([SequenceMatcher(None, crop_line, ref_line).ratio() for ref_line in ref_lines]))
        return sum(output) / float((len(ref_lines) * len(lines)))


def get_clubs_references(clubs, ref_clubs):
    # type: (list, DataFrame) -> list
    """
    Returns a list of reference sofifa id for a given tupled clubs list [(name, league),].

    :rtype: list
    :param clubs: tupled clubs list  [(name, league),].
    :param ref_clubs: reference clubs DataFrame.
    :return: reference id list.
    """

    def _f(leag):
        # type: (str) -> list
        """
        Returns a list of tupled (club_name, ref_id) for a given league.

        :rtype: list
        :param leag: league name.
        :return: list of tupled (club_name, ref_id).
        """
        refs = ref_clubs[ref_clubs["league"].apply(lambda x: leag == x)].values
        return refs[:, 0:2]

    sim = get_similarity
    get_ref = lists.get_nlt_max
    output = [get_ref([(sim(club_name, (ref_name,), split_ref=True, split_source=True), ref_id)
                       for ref_name, ref_id in _f(league)], pos_max=0, pos_ret=1, threshold=0)
              for club_name, league in clubs]
    for i, (club, league) in enumerate(clubs):
        logger.debug("{} -> {}".format(club, ref_clubs[ref_clubs["sofifa_id"] == output[i]]["name"].values[0]))
    return output


def match_clubs(sqlite_db, data):
    # type: (sqlite3, DataFrame) -> DataFrame
    """
    Matches clubs with sofifa data.

    :rtype: DataFrame
    :param sqlite_db: SQLite DB connection.
    :param data: matches DataFrame.
    :return: matches DataFrame.
    """
    clubs_data = sqlite.read_data(sqlite_db, config.clubs_table)
    clubs = zip(data["home_team"].values, data["league"].values)
    clubs += zip(data["away_team"].values, data["league"].values)
    clubs = lists.unique(clubs)
    clubs_refs = get_clubs_references(clubs, clubs_data)
    clubs = ["{} from {}".format(club_name, league) for club_name, league in clubs]
    refs_dict = dict(zip(clubs, clubs_refs))
    data["home_team_id"] = data.apply(
        lambda row: refs_dict["{} from {}".format(row["home_team"], row["league"])], axis=1)
    data["away_team_id"] = data.apply(
        lambda row: refs_dict["{} from {}".format(row["away_team"], row["league"])], axis=1)
    return data
