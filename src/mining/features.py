import numpy as np
import pandas as pd
from datetime import datetime


def calculate_results(data):
    # type: (DataFrame) -> DataFrame
    """
    Calculates results columns for each match into DataFrame.

    :rtype: DataFrame
    :param data: matches DataFrame.
    :return: matches DataFrame.
    """
    get_str_result = lambda x: "home_win" if x["home_goal"] > x["away_goal"] \
        else "home_draw" if x["home_goal"] == x["away_goal"] else "home_loss"
    get_num_result = lambda x: 1 if x["home_goal"] > x["away_goal"] \
        else 0 if x["home_goal"] == x["away_goal"] else -1
    result = data.apply(get_str_result, axis=1)
    one_hot = pd.get_dummies(result)
    data = data.join(one_hot)
    one_hot.rename(columns={"home_win": "away_loss", "home_draw": "away_draw", "home_loss": "away_win"}, inplace=True)
    data = data.join(one_hot)
    data["result"] = data.apply(get_num_result, axis=1)
    for col in ["home_win", "away_loss", "home_draw", "away_draw", "home_loss", "away_win"]:
        if col not in data.columns:
            data[col] = 0
    return data


def create_season_table(season):
    # type: (DataFrame) -> DataFrame
    """
    Creates season table regarding season results DataFrame.

    :rtype: DataFrame
    :param season: season results.
    :return: season table.
    """
    teams = np.unique(season[["home_team_id", "away_team_id"]].values.flatten())
    teams = pd.DataFrame(teams, columns=["team_id"])
    g = season.groupby("home_team_id")
    home = pd.DataFrame({"home_goals": g["home_goal"].sum(),
                         "home_goals_against": g["away_goal"].sum(),
                         "home_wins": g["home_win"].sum(),
                         "home_draws": g["home_draw"].sum(),
                         "home_losses": g["home_loss"].sum()
                         }).reset_index()
    g = season.groupby("away_team_id")
    away = pd.DataFrame({"away_goals": g["away_goal"].sum(),
                         "away_goals_against": g["home_goal"].sum(),
                         "away_wins": g["away_win"].sum(),
                         "away_draws": g["away_draw"].sum(),
                         "away_losses": g["away_loss"].sum()
                         }).reset_index()
    df = pd.merge(teams, home, how="left", left_on="team_id", right_on="home_team_id")
    df = pd.merge(df, away, how="left", left_on="team_id", right_on="away_team_id")
    df.fillna(0, inplace=True)
    # region calculate fields
    df["wins"] = df["home_wins"] + df["away_wins"]
    df["draws"] = df["home_draws"] + df["away_draws"]
    df["losses"] = df["home_losses"] + df["away_losses"]
    df["points"] = df["wins"] * 3 + df["draws"]
    df["gf"] = df["home_goals"] + df["away_goals"]
    df["ga"] = df["home_goals_against"] + df["away_goals_against"]
    df["gd"] = df["gf"] - df["ga"]
    df.reset_index(inplace=True)
    df = df.sort_values(by=["points", "gd"], ascending=False)
    df = df.reset_index()
    df["position"] = df.index + 1
    # endregion
    return df


def get_club_players(players_data, club_id, date):
    # type: (DataFrame, int, datetime) -> DataFrame
    """
    Returns players DataFrame for a given club id and date.

    :rtype: DataFrame
    :param players_data: players DataFrame.
    :param club_id: club id.
    :param date: date.
    :return: current club players DataFrame.
    """
    players_in_team = players_data[(players_data["team_id"] == club_id) & (players_data["version"] < date)]
    players_in_team = players_in_team.groupby("sofifa_id").first().reset_index()
    players_id = players_in_team["sofifa_id"].values
    players_everywhere = players_data[(players_data["sofifa_id"].isin(players_id)) & (players_data["version"] < date)]
    players_everywhere = players_everywhere.groupby("sofifa_id").first().reset_index()
    players = pd.merge(players_in_team, players_everywhere[["sofifa_id", "team_id"]],
                       how="left", on=["sofifa_id"])
    return players[players["team_id_x"] == players["team_id_y"]]


def calculate_composite_features(data):
    # type: (DataFrame) -> DataFrame
    """
    Calculates additional composite for each match into DataFrame.

    :rtype: DataFrame
    :param data: matches DataFrame.
    :return: matches DataFrame.
    """
    data["diff_form"] = data["home_form"] - data["away_form"]
    data["diff_position"] = data["home_position"] - data["away_position"]
    data["home_hazard"] = data["home_goal_a5"] + data["away_goal_p5"]
    data["away_hazard"] = data["away_goal_a5"] + data["home_goal_p5"]
    data["home_power"] = data["home_rat_att"] + data["home_rat_mid"] + data["home_rat_def"] + data["home_rat_gk"]
    data["away_power"] = data["away_rat_att"] + data["away_rat_mid"] + data["away_rat_def"] + data["away_rat_gk"]
    data["diff_power"] = data["home_power"] - data["away_power"]
    data["diff_fresh"] = data["home_fresh"] - data["away_fresh"]
    return data
