import config
import numpy as np
import modules.sqlitedb as sqlite
from sklearn.pipeline import Pipeline
from catboost import CatBoostClassifier
from modules.logering import setup_logger
from algorithms import calculate_kelly_result
from modules.timing import elapsed_timer, timeit
from sklearn.model_selection import train_test_split
from utils import get_random_params, get_params, save_search_results
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "searching kelly", title=True)
def search_kelly(train_data, tune_data):
    # type: (DataFrame, DataFrame) -> None
    """
    Searches parameters for Kelly betting algorithm.

    :rtype: None
    :param train_data: matches DataFrame for training classifier.
    :param tune_data: matches DataFrame for tuning betting parameters.
    :return: None.
    """
    clf_param_grid = {"iterations": [80, 100, 120],
                      "learning_rate": [0.025, 0.03, 0.035]}
    bet_param_grid = {"min_share": np.linspace(0, 0.1, num=1000),
                      "max_odd": np.linspace(1.2, 15.0, num=1000)}
    odds = tune_data[config.odds_cols].values
    x_train, y_train = train_data[config.train_features].values, 1 - train_data["result"].values
    x_tune, y_tune = tune_data[config.train_features].values, 1 - tune_data["result"].values

    output = []
    clf_params = get_params(clf_param_grid)
    bet_params = get_random_params(bet_param_grid, config.bet_random_tune)
    for i, clf_param_dict in enumerate(clf_params):
        catboost_params = config.catboost_params
        catboost_params.update(clf_param_dict)
        with elapsed_timer(logger, "checking clf params #{} of {}".format(i + 1, len(clf_params)), title=True):
            with elapsed_timer(logger, "training classifier", title=True):
                pipeline = Pipeline([("clf", CatBoostClassifier(**catboost_params))])
                pipeline.fit(x_train, y_train)
            with elapsed_timer(logger, "getting predictions", title=True):
                probs = pipeline.predict_proba(x_tune)
            with elapsed_timer(logger, "tuning betting params", title=True):
                for j, bet_param_dict in enumerate(bet_params):
                    kelly_stake = config.stake/bet_param_dict["min_share"]
                    results = calculate_kelly_result(probs, odds, y_tune, kelly_stake, **bet_param_dict)
                    results["clf_params"], results["bet_params"] = clf_param_dict, bet_param_dict
                    output.append(results)
    save_search_results(output, config.search_kelly_table)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=config.test_size, shuffle=False, random_state=config.seed)
    train_data, tune_data = train_test_split(data, test_size=config.tune_size, shuffle=False, random_state=config.seed)
    search_kelly(train_data, tune_data)


if __name__ == "__main__":
    main()
