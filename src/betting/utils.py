import config
import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import modules.sqlitedb as sqlite
import matplotlib. dates as mdates
from datetime import datetime
from modules.timing import timeit
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_random_params(param_grid, num_params):
    # type: (dict, int) -> list
    """
    Returns list of random parameters dictionaries for a given parameters grid.

    :rtype: list
    :param param_grid: parameters grid dictionary.
    :param num_params: number of random parameters.
    :return: list of random parameters dictionaries.
    """
    keys = param_grid.keys()
    params = list(itertools.product(*param_grid.values()))
    indices = np.random.choice(range(len(params)), num_params, replace=False).astype(int)
    params = [{key: param for key, param in zip(keys, param_set)} for param_set in np.array(params)[indices]]
    return params


def get_params(param_grid):
    # type: (dict, int) -> list
    """
    Returns list of parameters dictionaries for a given parameters grid.

    :rtype: list
    :param param_grid: parameters grid dictionary.
    :return: list of random parameters dictionaries.
    """
    keys = param_grid.keys()
    params = list(itertools.product(*param_grid.values()))
    params = [{key: param for key, param in zip(keys, param_set)} for param_set in params]
    return params


@timeit(logger, "saving search results", title=True)
def save_search_results(results, table_name):
    # type: (list, str) -> None
    """
    Saves betting search results to SQLite DB.

    :rtype: None
    :param results: list of searching results dictionaries.
    :param table_name: SQLite DB table name.
    :return: None.
    """
    df = pd.DataFrame(results)
    df["r^2_n"] = (df["r^2"] - df["r^2"].min()) / (df["r^2"].max() - df["r^2"].min())
    df["rate_n"] = (df["rate"] - df["rate"].min()) / (df["rate"].max() - df["rate"].min())
    df["score"] = (df["r^2_n"] + df["rate_n"]) / 2.
    df = pd.concat([df.drop(["clf_params"], axis=1), df["clf_params"].apply(pd.Series)], axis=1)
    df = pd.concat([df.drop(["bet_params"], axis=1), df["bet_params"].apply(pd.Series)], axis=1)
    sdb = sqlite.get_connection()
    sqlite.drop_table(sdb, table_name)
    sqlite.insert_data(sdb, table_name, df)
    sqlite.sort_data(sdb, table_name, ["score"], ["DESC"])


@timeit(logger, "building balance plot", title=True)
def build_balance_plot(data, result, filename):
    # type: (DataFrame, dict, str) -> None
    """
    Builds and saves balance plot.

    :rtype: None
    :param data: matches DataFrame
    :param result: betting result dictionary.
    :param filename: filename to save.
    :return: None.
    """
    dates = map(lambda d: datetime.utcfromtimestamp(d.tolist() / 1e9), data["date"].values)
    fig, ax = plt.subplots(1)
    fig.autofmt_xdate()
    plt.plot(dates, result["history"]["balance"])
    xfmt = mdates.DateFormatter("%d-%m-%Y")
    ax.xaxis.set_major_formatter(xfmt)
    plt.grid()
    plt.title("balance propagation over time")
    plt.xlabel("matches")
    plt.ylabel("income")
    plt.savefig("{}.png".format(filename), dpi=300)


@timeit(logger, "creating betting report", title=True)
def betting_report(result, filename):
    # type: (dict, str) -> None
    """
    Outputs betting report.

    :rtype: None
    :param result: betting results.
    :param filename: filename to save.
    :return: None.
    """
    result["history"].to_csv("{}.csv".format(filename), index=False, float_format="%.3f")
    result["accuracy"] *= 100
    for key, value in result.iteritems():
        if key != "history":
            logger.info("{}: {:.3f}".format(key, value))
