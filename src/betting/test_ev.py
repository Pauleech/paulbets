import os
import config
import pandas as pd
import modules.sqlitedb as sqlite
from modules.timing import timeit
from sklearn.externals import joblib
from modules.logering import setup_logger
from algorithms import calculate_ev_result
from utils import betting_report, build_balance_plot
from sklearn.model_selection import train_test_split
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "testing ev", title=True)
def test_ev(data, estimator):
    # type: (DataFrame, object) -> None
    """
    Tests EV betting algorithm.

    :rtype: None
    :param data: matches DataFrame
    :param estimator: trained classifier.
    :return: None.
    """
    x, y = data[config.train_features].values, 1 - data["result"].values
    odds = data[config.odds_cols].values
    probs = estimator.predict_proba(x)
    result = calculate_ev_result(probs, odds, y, config.stake, config.min_ev, config.max_odd, keep_history=True)
    filename = os.path.join(config.betting_history_folder, "catboost-ev-main")
    betting_report(result, filename)
    build_balance_plot(data, result, filename)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=config.test_size, shuffle=False, random_state=config.seed)
    test_data["date"] = pd.to_datetime(test_data["date"])
    clf_name = os.path.join(config.models_folder, "catboost-main.pickle")
    clf = joblib.load(clf_name)
    test_ev(test_data, clf)


if __name__ == "__main__":
    main()
