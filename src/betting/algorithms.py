import config
import numpy as np
import pandas as pd
from collections import OrderedDict
from sklearn.metrics import r2_score
from modules.logering import setup_logger
from sklearn.linear_model import LinearRegression
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_linear_stats(balances):
    # type: (ndarray) -> (float, float)
    """
    Returns balance linear regression growth rate and r^2 score for a given sequence.

    :rtype: float, float
    :param balances: balances sequence.
    :return: balance linear regression growth rate and r^2 score.
    """
    model = LinearRegression(fit_intercept=False)
    x = np.linspace(0, len(balances) - 1, len(balances))
    model.fit(np.expand_dims(x, 1), balances)
    y_pred = model.predict(np.expand_dims(x, 1))
    return model.coef_[0], r2_score(balances, y_pred)


def get_betting_history(bets, odds, wager, result, balances):
    # type: (ndarray, ndarray, ndarray, ndarray, ndarray) -> DataFrame
    """
    Returns betting history DataFrame.

    :rtype: DataFrame
    :param bets: made bets.
    :param odds: betting odds.
    :param wager: possible incomes.
    :param result: matches outcomes.
    :param balances: balances sequence.
    :return: betting history DataFrame.
    """
    columns = ["bet_home", "bet_draw", "bet_away",
               "odds_home", "odds_draw", "odds_away",
               "wager_home", "wager_draw", "wager_away",
               "result_home", "result_draw", "result_away", "balance"]
    history = np.hstack((bets, odds, wager, result, np.expand_dims(balances, 1)))
    df = pd.DataFrame(history, columns=columns)
    return df


def calculate_ev_result(probs, odds, y_eval, stake, min_ev, max_odd, keep_history=False):
    # type: (ndarray, ndarray, ndarray, float, float, float, bool) -> dict
    """
    Calculates EV betting algorithm result.

    :rtype: dict
    :param probs: outcomes probabilities.
    :param odds: betting odds.
    :param y_eval: evaluation labels.
    :param stake: fixed betting stake (bank share).
    :param min_ev: minimal expected value to make a bet.
    :param max_odd: maximal bookmaker odd to make a bet.
    :param keep_history: keep betting history in the output.
    :return: algorithm results dictionary.
    """
    output = OrderedDict()
    result = np.array([y_eval == 0,
                       y_eval == 1,
                       y_eval == 2]).transpose().astype(float)
    ev = probs * (odds - 1) * stake - (1 - probs) * stake
    bets_conf = (ev > min_ev).astype(int)
    vec = np.vectorize(lambda x: 0 if x >= max_odd else 1, otypes=[np.float])
    longshots = vec(odds).astype(float)
    bets = stake * longshots * bets_conf
    wager = bets * odds * result
    stakes = (longshots * bets_conf).ravel()
    balances = np.cumsum(np.sum(-bets + wager, axis=1))
    output["income"] = balances[-1]
    output["num_bets"] = sum(stakes)
    output["num_matches"] = len(probs)
    output["accuracy"] = sum((stakes.ravel() == 1) * (result.ravel() == 1)) / sum(stakes) if sum(stakes) != 0 else 0
    output["rate"], output["r^2"] = get_linear_stats(balances)
    if keep_history:
        output["history"] = get_betting_history(bets, odds, wager, result, balances)
    return output


def calculate_kelly_result(probs, odds, y_eval, max_stake, min_share, max_odd, keep_history=False):
    # type: (ndarray, ndarray, ndarray, float, float, float, bool) -> dict
    """
    Calculates Kelly betting algorithm result.

    :rtype: dict
    :param probs: outcomes probabilities.
    :param odds: betting odds.
    :param y_eval: evaluation labels.
    :param max_stake: max betting stake (bank share).
    :param min_share: minimal stake share to make a bet.
    :param max_odd: maximal bookmaker odd to make a bet.
    :param keep_history: keep betting history in the output.
    :return: algorithm results dictionary.
    """
    output = OrderedDict()
    result = np.array([y_eval == 0,
                       y_eval == 1,
                       y_eval == 2]).transpose().astype(float)
    stake = (probs * (odds - 1) - (1 - probs)) / (odds - 1.)
    bets_conf = (stake > min_share).astype(int)
    vec = np.vectorize(lambda x: 0 if x >= max_odd else 1, otypes=[np.float])
    longshots = vec(odds).astype(float)
    bets = stake * longshots * bets_conf * max_stake
    wager = bets * odds * result
    stakes = (longshots * bets_conf).ravel()
    balances = np.cumsum(np.sum(-bets + wager, axis=1))
    output["income"] = balances[-1]
    output["num_bets"] = sum(stakes)
    output["num_matches"] = len(probs)
    output["accuracy"] = sum((stakes.ravel() == 1) * (result.ravel() == 1)) / sum(stakes) if sum(stakes) != 0 else 0
    output["rate"], output["r^2"] = get_linear_stats(balances)
    if keep_history:
        output["history"] = get_betting_history(bets, odds, wager, result, balances)
    return output
