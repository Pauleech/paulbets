import os
import config
import numpy as np
import modules.sqlitedb as sqlite
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.decomposition import PCA
from modules.dateutils import get_datestr
from sklearn.pipeline import Pipeline
from catboost import CatBoostClassifier
from modules.logering import setup_logger
from sklearn.model_selection import KFold
from sklearn.naive_bayes import GaussianNB
from modules.timing import elapsed_timer, timeit
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from mlxtend.classifier import EnsembleVoteClassifier
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "training voting classifier", title=True)
def search_ensemble(x, y):
    # type: (ndarray, ndarray) -> None
    """
    Searches parameters for voting classifier.

    :rtype: None
    :param x: train features.
    :param y: train labels.
    :return: None.
    """
    svm_params = {"C": config.svm_c, "gamma": config.svm_gamma,
                  "random_state": config.seed, "probability": True}
    pca_params = {"n_components": config.pca_evr, "svd_solver": "full"}
    kcv = KFold(n_splits=config.cv_train)
    num_weights = 1000
    weights_dict = {tuple(weights): np.zeros(config.cv_train) for weights in np.random.rand(num_weights, 3)}
    for i, (train_index, test_index) in enumerate(kcv.split(x)):
        x_train, x_test = x[train_index], x[test_index]
        y_train, y_test = y[train_index], y[test_index]
        clf1 = Pipeline([("clf", CatBoostClassifier(**config.catboost_params))])
        clf2 = Pipeline([("scaler", StandardScaler()), ("pca", PCA(**pca_params)), ("clf", GaussianNB())])
        clf3 = Pipeline([("scaler", StandardScaler()), ("clf", SVC(**svm_params))])
        with elapsed_timer(logger, "training classifiers", title=True):
            clf1.fit(x_train, y_train)
            clf2.fit(x_train, y_train)
            clf3.fit(x_train, y_train)
        for weights in weights_dict.keys():
            pipeline = EnsembleVoteClassifier(clfs=[clf1, clf2, clf3], voting="soft", weights=weights, refit=False)
            pipeline.fit(x_train, y_train)
            y_pred = pipeline.predict(x_test)
            weights_dict[weights][i] = metrics.accuracy_score(y_test, y_pred)
    mean_dict = {k: v.mean() for k, v in weights_dict.items()}
    std_dict = {k: v.std() for k, v in weights_dict.items()}
    filename = os.path.join(config.prediction_params_folder, "{}.txt".format(get_datestr()))
    with open(filename, "w") as f:
        for key in sorted(mean_dict, key=mean_dict.get, reverse=True):
            f.write("({}, {}, {})\n".format(mean_dict[key], std_dict[key], key))


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=1 - config.train_size, shuffle=False, random_state=config.seed)
    x, y = data[config.train_features].values, 1 - data["result"].values
    search_ensemble(x, y)


if __name__ == "__main__":
    main()
