import config
import numpy as np
from sklearn import metrics
from modules.logering import setup_logger
from sklearn.model_selection import TimeSeriesSplit
logger = setup_logger(__name__, config.log_name, config.log_level)


def show_bookmaker_baseline(data):
    # type: (DataFrame) -> None
    """
    Prints bookmaker "baseline" performance.

    :rtype: None
    :param data: matches DataFrame.
    :return: None.
    """
    tscv = TimeSeriesSplit(n_splits=config.cv_train)
    y_true_folds = []
    y_pred_folds = []
    for i, (_, test_index) in enumerate(tscv.split(data)):
        books = data.iloc[test_index][["B365H", "B365D", "B365A"]].copy()
        y_true = 1 - data.iloc[test_index]["result"].values
        books["res"] = books.values.tolist()
        books["res"] = books["res"].apply(lambda v: np.array(v))
        y_pred = books["res"].apply(lambda v: np.random.choice(np.where(v == v.min())[0])).values
        y_true_folds.append(y_true)
        y_pred_folds.append(y_pred)
    logger.info("bookmaker baseline accuracy: ")
    classification_report(y_true_folds, y_pred_folds)


def classification_report(y_true_folds, y_pred_folds):
    # type: (list, list) -> None
    """
    Evaluates result for given true and predicted labels.

    :rtype: None
    :param y_true_folds: list of true labels folds.
    :param y_pred_folds: list of predicted labels folds.
    :return: None.
    """
    cms = np.zeros((len(y_true_folds), 3, 3))
    acs = np.zeros(len(y_true_folds))
    for i, (y_true, y_pred) in enumerate(zip(y_true_folds, y_pred_folds)):
        cm = metrics.confusion_matrix(y_true, y_pred)
        cms[i] = cm / cm.astype(np.float).sum(keepdims=True)
        acs[i] = metrics.accuracy_score(y_true, y_pred)
    logger.info("confusion matrix (home, draw, away):\n{}".format(100 * cms.mean(axis=0)))
    logger.info("folds accuracies: {}".format(100 * acs))
    logger.info("accuracy: {:.3f}%".format(100 * acs.mean()))
