import os
import math
import config
import itertools
import numpy as np
from operator import mul
from modules.dateutils import get_datestr
from modules.timing import timeit
from modules.logering import setup_logger
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, TimeSeriesSplit
logger = setup_logger(__name__, config.log_name, config.log_level)


class BanyanSearch:
    def __init__(self, x, y, pipeline, restrictions, scoring, set_size=5, cv=2,
                 debug=False, tree_depth=4, tree_leafs=(2, 2, 1, 1),
                 random_iter_pre=100, random_best_pre=5,
                 random_iter_post=5, random_best_post=5, banyan_params=None):
        # type: (ndarray, ndarray, object, dict, str, int, int, bool, int, tuple, int, int, int, int, tuple) -> None
        """
        Initiates BanyanSearch object.

        :rtype: None
        :param x: train features.
        :param y: train labels.
        :param pipeline: a pipeline of transforms with a final pipeline.
        :param restrictions: parameters restrictions dictionary.
        :param scoring: validation scoring.
        :param set_size: parameters sets size.
        :param cv: the number of folds for cross validation.
        :param debug: debug state (use dummy generation of search params).
        :param tree_depth: parameters tree depth.
        :param tree_leafs: parameters tree number of leafs list for tree expanding.
        :param random_iter_pre: number of random search iterations for presearch.
        :param random_best_pre: number of best sets from random search to use for presearch.
        :param random_iter_post: number of random search iterations for postsearch.
        :param random_best_post: number of best sets from random search to use for postsearch.
        :param banyan_params: float parameters to determine in tree-expand manner (other params will be fixed).
        :return: None.
        """
        self.x = x
        self.y = y
        self.pipeline = pipeline
        self.restrictions = restrictions
        self.scoring = scoring
        self.set_size = set_size
        self.cv = cv
        self.debug = debug
        self.tree_depth = tree_depth
        self.tree_leafs = tree_leafs
        self.random_iter_pre = random_iter_pre
        self.random_best_pre = random_best_pre
        self.random_iter_post = random_iter_post
        self.random_best_post = random_best_post
        self.banyan_params = banyan_params
        self.search_results = []

    def log_num_searches(self, param_grid=None):
        # type: (dict) -> None
        """
        Logs number of searches for full banyan search.

        :rtype: None
        :param param_grid: parameters grid dictionary.
        :return: None.
        """
        if self.banyan_params is None:
            num_searches = reduce(mul, [len(values) for values in param_grid.itervalues()]) * self.cv
            logger.info("overall number of searches: {}.".format(num_searches))
        else:
            pre_searches = self.cv * self.random_iter_pre
            logger.info("number of random presearches: {}.".format(pre_searches))
            tree_searches = []
            prev = int(math.pow(self.set_size, len(self.banyan_params))) * self.random_best_pre * self.cv
            tree_searches.append(prev)
            logger.info("tree layer #{}, number of searches: {}.".format(len(tree_searches) - 1, prev))
            for value in self.tree_leafs[:-1]:
                prev *= value
                tree_searches.append(prev)
                logger.info("tree layer #{}, number of searches: {}.".format(len(tree_searches) - 1, prev))
            post_searches = self.cv * self.random_iter_post
            post_searches *= self.random_best_pre * reduce(lambda x, y: x * y, self.tree_leafs)
            logger.info("number of random postsearches: {}.".format(post_searches))
            logger.info("overall number of searches: {}.".format(pre_searches + sum(tree_searches) + post_searches))

    def append_search_results(self, results):
        # type: (list) -> None
        """
        Appends and sort search params class variable.

        :rtype: None
        :param results: list of sorted tuples: (mean, std, params).
        :return: None.
        """
        results = [(mean, std, params) for mean, std, params in results]
        self.search_results = sorted(itertools.chain(self.search_results, results),
                                     key=lambda i: i[0], reverse=True)

    def save_search_results(self, path):
        # type: (str) -> None
        """
        Saves search params class variable.

        :rtype: None
        :param path: path to save.
        :return: None.
        """
        filename = os.path.join(path, "{}.txt".format(get_datestr()))
        lines = ["{}\n".format(str(result)) for result in self.search_results]
        with open(filename, "w") as f:
            # region writing model info
            f.write("{}\n".format(str(self.pipeline)))
            f.write("CV: {};\n".format(str(self.cv)))
            f.write("Set size: {};\n".format(str(self.set_size)))
            f.write("Tree depth: {};\n".format(str(self.tree_depth)))
            f.write("Tree leafs: {}\n".format(str(self.tree_leafs)))
            f.write("Random presearch iterations: {};\n".format(str(self.random_iter_pre)))
            f.write("Random presearch best count: {};\n".format(str(self.random_best_pre)))
            f.write("Random postsearch iterations: {};\n".format(str(self.random_iter_post)))
            f.write("Random postsearch best count: {};\n".format(str(self.random_best_post)))
            f.write("Restrictions: {}\n".format(str(self.restrictions)))
            f.write("\nSearch params:\n")
            # endregion
            f.writelines(lines)

    @staticmethod
    def get_param_set(value, set_size, set_range, restrictions):
        # type: (float, int, float, list) -> ndarray
        """
        Returns parameters set for given parameter center value, set range and restrictions.
        Example: value = 5, set_size = 5, set_range = 5 -> array([2.5, 3.75, 5, 6.25, 7.25])

        :rtype: ndarray
        :param value: center value for parameters set.
        :param set_size: number of values in parameters set.
        :param set_range: set range.
        :param restrictions: parameters restrictions: [bottom, upper].
        :return: parameters set.
        """
        res_bottom, res_upper = restrictions
        bottom = value - set_range / 2.
        upper = value + set_range / 2.
        sz = set_size if (bottom > res_bottom and upper < res_upper) else set_size - 1
        bottom = bottom if bottom >= res_bottom else res_bottom
        upper = upper if upper <= res_upper else res_upper
        output = np.linspace(bottom, upper, sz)
        if sz == set_size - 1:
            output = np.insert(output, sz / 2, [value])
        return output

    @staticmethod
    def expand_param_set(param_set, set_size):
        # type: (ndarray, int) -> ndarray
        """
        Returns new parameter search set by expanding given parameter set and number of samples.

        :rtype: ndarray
        :param param_set: parameters set ndarray.
        :param set_size: size of new parameters set.
        :return: new parameter search set.
        """
        best = param_set[2]
        output = np.linspace(param_set[0], param_set[1], set_size - 1)
        output = np.append(output, best)
        return output

    @staticmethod
    def get_dummy_random_search_result(param_grid, random_iter):
        # type: (dict, int) -> list
        """
        Returns dummy generated RandomSearchCV result as list of sorted tuples: (mean, std, params).

        :rtype: list
        :param param_grid: parameters grid dictionary.
        :param random_iter: number of random search iterations.
        :return: RandomSearchCV result: list of sorted tuples: (mean, std, params).
        """
        params = []
        for values in param_grid.values():
            params.append(values)
        params = list(itertools.product(*params))
        keys = param_grid.keys()
        params = [{key: value for (key, value) in zip(keys, param)} for param in params]
        params = np.random.choice(params, random_iter, replace=False)
        means = np.random.uniform(low=0.0, high=100.0, size=len(params))
        stds = np.random.uniform(low=0.0, high=10.0, size=len(params))
        return sorted(zip(means, stds, params), key=lambda i: i[0], reverse=True)

    def get_random_search_result(self, param_grid, random_iter):
        # type: (dict, int) -> list
        """
        Returns RandomSearchCV result as list of sorted tuples: (mean, std, params).

        :rtype: list
        :param param_grid: parameters grid dictionary.
        :param random_iter: number of random search iterations.
        :return: RandomSearchCV result: list of sorted tuples: (mean, std, params).
        """
        cv = TimeSeriesSplit(n_splits=self.cv).split(self.x)
        gsearch = RandomizedSearchCV(estimator=self.pipeline, param_distributions=param_grid, cv=cv,
                                     random_state=config.seed, scoring=self.scoring, iid=False,
                                     verbose=0, n_iter=random_iter)
        gsearch.fit(self.x, self.y)
        means = gsearch.cv_results_["mean_test_score"]
        stds = gsearch.cv_results_["std_test_score"]
        params = gsearch.cv_results_["params"]
        return sorted(zip(means, stds, params), key=lambda i: i[0], reverse=True)

    @timeit(logger, "performing parameters random search", title=True)
    def param_random_search(self, params, root):
        # type: (list, bool) -> list
        """
        Searches for the best parameters sets in a random manner.

        :rtype: list
        :param params: list of parameters grid dictionaries.
        :param root: defines presearch or postsearch.
        :return: a list of best parameters intervals: [dict()].
        """
        output = []
        get_results = self.get_random_search_result if not self.debug else self.get_dummy_random_search_result
        random_best = self.random_best_pre if root else self.random_best_post
        random_iter = self.random_iter_pre if root else self.random_iter_post
        for param_index, param_grid in enumerate(params):
            random_search_results = get_results(param_grid, random_iter)[:random_best]
            self.append_search_results(random_search_results)
            for best_index, result in enumerate(random_search_results):
                logger.info("params #{}, best #{}: {}".format(param_index, best_index, result))
                params = dict()
                for key, value in result[2].iteritems():
                    if self.banyan_params is None:
                        params[key] = self.get_param_set(value, self.set_size, value, self.restrictions[key])
                    else:
                        if key in self.banyan_params:
                            params[key] = self.get_param_set(value, self.set_size, value, self.restrictions[key])
                        else:
                            params[key] = param_grid[key]
                output.append(params)
        return output

    @staticmethod
    def get_dummy_grid_search_result(param_grid):
        # type: (dict) -> list
        """
        Returns dummy generated GridSearchCV result as list of sorted tuples: (mean, std, params).

        :rtype: list
        :param param_grid: parameters grid dictionary.
        :return: GridSearchCV result: list of sorted tuples: (mean, std, params).
        """
        params = []
        for values in param_grid.values():
            params.append(values)
        params = list(itertools.product(*params))
        keys = param_grid.keys()
        params = [{key: value for (key, value) in zip(keys, param)} for param in params]
        means = np.random.uniform(low=0.0, high=100.0, size=len(params))
        stds = np.random.uniform(low=0.0, high=10.0, size=len(params))
        return sorted(zip(means, stds, params), key=lambda i: i[0], reverse=True)

    def get_grid_search_result(self, param_grid):
        # type: (dict) -> list
        """
        Returns GridSearchCV result as list of sorted tuples: (mean, std, params).

        :rtype: list
        :param param_grid: parameters grid dictionary.
        :return: GridSearchCV result: list of sorted tuples: (mean, std, params).
        """
        cv = TimeSeriesSplit(n_splits=self.cv).split(self.x)
        gsearch = GridSearchCV(estimator=self.pipeline, param_grid=param_grid, cv=cv,
                               scoring=self.scoring, iid=False, verbose=0)
        gsearch.fit(self.x, self.y)
        means = gsearch.cv_results_["mean_test_score"]
        stds = gsearch.cv_results_["std_test_score"]
        params = gsearch.cv_results_["params"]
        return sorted(zip(means, stds, params), key=lambda i: i[0], reverse=True)

    @timeit(logger, "performing parameters tree search", title=True)
    def param_tree_search(self, params):
        # type: (list) -> list
        """
        Searches for the best parameters sets in a tree-expand manner.

        :rtype: list
        :param params: list of parameters grid dictionaries.
        :return: a list of expanded best parameters intervals: [dict()].
        """
        output = []
        tree = {"children": [{"params": param_grid, "children": list()} for param_grid in params]}
        get_results = self.get_grid_search_result if not self.debug else self.get_dummy_grid_search_result
        prev_layer = tree["children"]
        for depth in range(self.tree_depth):
            next_layer = []
            for branch, node in enumerate(prev_layer):
                params = node["params"]
                best_results = get_results(params)[:self.tree_leafs[depth]]
                self.append_search_results(best_results)
                for leaf, result in enumerate(best_results):
                    logger.info("depth #{}, branch #{}, leaf #{} : {}".format(depth, branch, leaf, result))
                    child = {"score": result[0], "params": dict(), "children": list()}
                    for key, value in result[2].iteritems():
                        if key in self.banyan_params:
                            diff = params[key][1] - params[key][0]
                            child["params"][key] = self.get_param_set(value, self.set_size,
                                                                      diff, self.restrictions[key])
                        else:
                            child["params"][key] = params[key]
                    node["children"].append(child)
                    next_layer.append(child)
            prev_layer = next_layer
        for d in prev_layer:
            for key, value in d["params"].items():
                if key in self.banyan_params:
                    d["params"][key] = self.expand_param_set(d["params"][key], self.random_iter_post*10)
            output.append(d["params"])
        return output

    @timeit(logger, "performing banyan search", title=True)
    def run_search(self, param_grid):
        # type: (dict) -> None
        """
        Searches for the best continuous parameters sets in a banyan-search manner.

        :rtype: None
        :param param_grid: parameters grid dictionary.
        :return: None.
        """
        if self.banyan_params is None:
            best_results = self.get_grid_search_result(param_grid)
            self.append_search_results(best_results)
        else:
            params = self.param_random_search([param_grid, ], root=True)
            params = self.param_tree_search(params)
            self.param_random_search(params, root=False)
