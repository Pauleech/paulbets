import config
import numpy as np
import modules.sqlitedb as sqlite
from sklearn.svm import SVC
from modules.timing import timeit
from sklearn.pipeline import Pipeline
from banyansearch import BanyanSearch
from modules.logering import setup_logger
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "training svm banyan", title=True)
def search_svm_banyan(x, y, scoring="accuracy"):
    # type: (ndarray, ndarray, str) -> None
    """
    Searches parameters for SVM via BanyanSearch.

    :rtype: None
    :param x: train features.
    :param y: train labels.
    :param scoring: validation scoring.
    :return: None.
    """
    pipeline = Pipeline([
        ("scaler", StandardScaler()),
        ("clf", SVC(random_state=config.seed))
    ])
    param_grid = {"clf__C": np.geomspace(0.0001, 100, num=10000),
                  "clf__gamma": np.geomspace(0.001, 10, num=10000)}
    restrictions = {"clf__C": [1e-5, 100], "clf__gamma": [1e-5, 100]}

    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post,
                     "banyan_params": ("clf__C", "clf__gamma")}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches()
    banyan.run_search(param_grid)
    banyan.save_search_results(config.prediction_params_folder)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=1 - config.train_size, shuffle=False, random_state=config.seed)
    x, y = data[config.train_features].values, 1 - data["result"].values
    scoring = "accuracy"
    search_svm_banyan(x, y, scoring)


if __name__ == "__main__":
    main()
