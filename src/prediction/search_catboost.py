import config
import numpy as np
import modules.sqlitedb as sqlite
from modules.timing import timeit
from sklearn.pipeline import Pipeline
from banyansearch import BanyanSearch
from catboost import CatBoostClassifier
from modules.logering import setup_logger
from sklearn.model_selection import train_test_split
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "searching border_count", title=True)
def search_border_count(x, y, pipeline, scoring):
    # type: (ndarray, ndarray, object, str) -> object
    """
    Searches through ctr_border_count parameters.

    :rtype: object
    :param x: train features.
    :param y: train labels.
    :param pipeline: pipeline object.
    :param scoring: validation scoring.
    :return: banyan object.
    """
    param_grid = {"clf__border_count": [32, 5, 10, 20, 50, 100, 150, 200]}
    restrictions = {}
    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches(param_grid)
    banyan.run_search(param_grid)
    return banyan


@timeit(logger, "searching ctr_border_count", title=True)
def search_ctr_border_count(x, y, pipeline, scoring):
    # type: (ndarray, ndarray, object, str) -> object
    """
    Searches through ctr_border_count parameters.

    :rtype: object
    :param x: train features.
    :param y: train labels.
    :param pipeline: pipeline object.
    :param scoring: validation scoring.
    :return: banyan object.
    """
    param_grid = {"clf__ctr_border_count": [50, 5, 10, 20, 100, 150, 200]}
    restrictions = {}
    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches(param_grid)
    banyan.run_search(param_grid)
    return banyan


@timeit(logger, "searching l2_leaf_reg", title=True)
def search_l2_leaf_reg(x, y, pipeline, scoring):
    # type: (ndarray, ndarray, object, str) -> object
    """
    Searches through l2_leaf_reg parameters.

    :rtype: object
    :param x: train features.
    :param y: train labels.
    :param pipeline: pipeline object.
    :param scoring: validation scoring.
    :return: banyan object.
    """
    param_grid = {"clf__l2_leaf_reg": [3, 1, 5, 10, 25, 50, 100]}
    restrictions = {}
    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches(param_grid)
    banyan.run_search(param_grid)
    return banyan


@timeit(logger, "searching iterations and learning_rate", title=True)
def search_iterations_and_rate(x, y, pipeline, scoring):
    # type: (ndarray, ndarray, object, str) -> object
    """
    Searches through iterations and learning_rate parameters.

    :rtype: object
    :param x: train features.
    :param y: train labels.
    :param pipeline: pipeline object.
    :param scoring: validation scoring.
    :return: banyan object.
    """
    param_grid = {"clf__iterations": [50, 100, 200, 500],
                  "clf__learning_rate": np.append(np.linspace(0.001, 0.3, 10000), [0.03])}
    restrictions = {"clf__learning_rate": [1e-4, 0.4]}
    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": 10,
                     "random_best_pre": 2,
                     "random_iter_post": 1,
                     "random_best_post": 1,
                     "banyan_params": ("clf__learning_rate",)}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches(param_grid)
    banyan.run_search(param_grid)
    return banyan


@timeit(logger, "searching depth", title=True)
def search_depth(x, y, pipeline, scoring):
    # type: (ndarray, ndarray, object, str) -> object
    """
    Searches through depth parameters.

    :rtype: object
    :param x: train features.
    :param y: train labels.
    :param pipeline: pipeline object.
    :param scoring: validation scoring.
    :return: banyan object.
    """
    param_grid = {"clf__depth": [3, 1, 2, 6, 4, 5, 7]}
    restrictions = {}
    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches(param_grid)
    banyan.run_search(param_grid)
    return banyan


@timeit(logger, "training catboost banyan", title=True)
def search_catboost_banyan(x, y, scoring="accuracy"):
    # type: (ndarray, ndarray, str) -> None
    """
    Searches parameters for Catboost via BanyanSearch.

    :rtype: None
    :param x: train features.
    :param y: train labels.
    :param scoring: validation scoring.
    :return: None.
    """
    get_pipeline = lambda cparams: Pipeline([("clf", CatBoostClassifier(**cparams))])
    get_best_params = lambda b: {k.replace("clf__", ""): v for k, v in b.search_results[0][2].iteritems()}
    searches = [search_border_count, search_ctr_border_count,
                search_l2_leaf_reg, search_iterations_and_rate, search_depth]
    catboost_params = {"loss_function": "MultiClass",
                       "random_seed": config.seed,
                       "allow_writing_files": False,
                       "iterations": 100}
    for search in searches:
        pipeline = get_pipeline(catboost_params)
        banyan = search(x, y, pipeline, scoring)
        best_params = get_best_params(banyan)
        logger.info("best: {}".format(banyan.search_results[0]))
        catboost_params.update(best_params)
    logger.info("best parameters: {}".format(catboost_params))


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=1 - config.train_size, shuffle=False, random_state=config.seed)
    x, y = data[config.train_features].values, 1 - data["result"].values
    scoring = "accuracy"
    search_catboost_banyan(x, y, scoring)


if __name__ == "__main__":
    main()
