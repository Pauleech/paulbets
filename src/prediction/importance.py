import config
import numpy as np
import modules.sqlitedb as sqlite
from modules.timing import timeit
from modules.logering import setup_logger
from sklearn.ensemble import ExtraTreesClassifier
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "calculating features importance", title=True)
def show_feature_importance(data):
    # type: (DataFrame) -> None
    """
    Shows features importance.

    :rtype: None
    :param data: matches DataFrame.
    :return: None.
    """
    x, y = data[config.train_features].values, 1 - data["result"].values
    forest = ExtraTreesClassifier(n_estimators=250, random_state=config.seed)
    forest.fit(x, y)
    importances = forest.feature_importances_
    indices = np.argsort(importances)[::-1]
    columns = data[config.train_features].columns
    logger.info("feature ranking:")
    for f in range(x.shape[1]):
        logger.info("{}. feature {} ({:.5f})".format(f + 1, columns[indices[f]], importances[indices[f]]))


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    show_feature_importance(data)


if __name__ == "__main__":
    main()
