import config
import modules.sqlitedb as sqlite
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from modules.logering import setup_logger
from sklearn.preprocessing import StandardScaler
from modules.timing import elapsed_timer, timeit
from sklearn.model_selection import TimeSeriesSplit
from prediction.utils import show_bookmaker_baseline, classification_report
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "testing svm", title=True)
def test_svm(x, y):
    # type: (ndarray, ndarray) -> None
    """
    Tests SVM classifier.

    :rtype: None
    :param x: features.
    :param y: labels.
    :return: None.
    """
    svm_params = {"C": config.svm_c, "gamma": config.svm_gamma, "random_state": config.seed}
    tscv = TimeSeriesSplit(n_splits=config.cv_train)
    y_pred_folds = []
    y_test_folds = []
    for train_index, test_index in tscv.split(x):
        x_train, x_test = x[train_index], x[test_index]
        y_train, y_test = y[train_index], y[test_index]
        y_test_folds.append(y_test)
        pipeline = Pipeline([
            ("scaler", StandardScaler()),
            ("clf", SVC(**svm_params))
        ])
        with elapsed_timer(logger, "training classifier", title=True):
            pipeline.fit(x_train, y_train)
        y_pred = pipeline.predict(x_test)
        y_pred_folds.append(y_pred)
    classification_report(y_test_folds, y_pred_folds)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    show_bookmaker_baseline(data)
    x, y = data[config.train_features].values, 1 - data["result"].values
    test_svm(x, y)


if __name__ == "__main__":
    main()
