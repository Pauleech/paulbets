import config
import numpy as np
import modules.sqlitedb as sqlite
from modules.timing import timeit
from sklearn.pipeline import Pipeline
from banyansearch import BanyanSearch
from sklearn.decomposition import PCA
from modules.logering import setup_logger
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "training gnb banyan", title=True)
def search_gnb_banyan(x, y, scoring="accuracy"):
    # type: (ndarray, ndarray, str) -> None
    """
    Searches parameters for GaussianNB via BanyanSearch.

    :rtype: None
    :param x: train features.
    :param y: train labels.
    :param scoring: validation scoring.
    :return: None.
    """
    pca_params = {"svd_solver": "full"}
    pipeline = Pipeline([
        ("scaler", StandardScaler()),
        ("pca", PCA(**pca_params)),
        ("clf", GaussianNB())
    ])
    param_grid = {"pca__n_components": np.linspace(0.1, 0.999, num=100)}
    restrictions = {"pca__n_components": [0.01, 0.9999]}

    search_params = {"set_size": config.banyan_set_size, "cv": config.cv_train,
                     "tree_depth": config.banyan_tree_depth, "tree_leafs": config.banyan_tree_leafs,
                     "random_iter_pre": config.banyan_random_iter_pre,
                     "random_best_pre": config.banyan_random_best_pre,
                     "random_iter_post": config.banyan_random_iter_post,
                     "random_best_post": config.banyan_random_best_post,
                     "banyan_params": ("pca__n_components", )}
    banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, **search_params)
    banyan.log_num_searches()
    banyan.run_search(param_grid)
    banyan.save_search_results(config.prediction_params_folder)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=1 - config.train_size, shuffle=False, random_state=config.seed)
    x, y = data[config.train_features].values, 1 - data["result"].values
    scoring = "accuracy"
    search_gnb_banyan(x, y, scoring)


if __name__ == "__main__":
    main()
