import os
import config
import modules.sqlitedb as sqlite
from sklearn.svm import SVC
from modules.timing import timeit
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from catboost import CatBoostClassifier
from modules.logering import setup_logger
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from mlxtend.classifier import EnsembleVoteClassifier
logger = setup_logger(__name__, config.log_name, config.log_level)


@timeit(logger, "training svm", title=True)
def train_svm(x_train, y_train):
    # type: (ndarray, ndarray) -> object
    """
    Trains SVM classifier.

    :rtype: object
    :param x_train: features.
    :param y_train: labels.
    :return: trained classifier.
    """
    svm_params = {"C": config.svm_c, "gamma": config.svm_gamma, "random_state": config.seed, "probability": True}
    pipeline = Pipeline([
        ("scaler", StandardScaler()),
        ("clf", SVC(**svm_params))
    ])
    pipeline.fit(x_train, y_train)
    model_name = os.path.join(config.models_folder, "svm-main.pickle")
    joblib.dump(pipeline, model_name, compress=1)
    return pipeline


@timeit(logger, "training gnb", title=True)
def train_gnb(x_train, y_train):
    # type: (ndarray, ndarray) -> object
    """
    Trains GaussianNB classifier.

    :rtype: object
    :param x_train: features.
    :param y_train: labels.
    :return: trained classifier.
    """
    pca_params = {"n_components": config.pca_evr, "svd_solver": "full"}
    pipeline = Pipeline([
        ("scaler", StandardScaler()),
        ("pca", PCA(**pca_params)),
        ("clf", GaussianNB())
    ])
    pipeline.fit(x_train, y_train)
    model_name = os.path.join(config.models_folder, "gnb-main.pickle")
    joblib.dump(pipeline, model_name, compress=1)
    return pipeline


@timeit(logger, "training catboost", title=True)
def train_catboost(x_train, y_train):
    # type: (ndarray, ndarray) -> object
    """
    Trains Catboost classifier.

    :rtype: object
    :param x_train: features.
    :param y_train: labels.
    :return: trained classifier.
    """
    pipeline = Pipeline([
        ("clf", CatBoostClassifier(**config.catboost_params))
    ])
    pipeline.fit(x_train, y_train)
    model_name = os.path.join(config.models_folder, "catboost-main.pickle")
    joblib.dump(pipeline, model_name, compress=1)
    return pipeline


@timeit(logger, "training voting classifier", title=True)
def train_ensemble(x_train, y_train, clfs):
    # type: (ndarray, ndarray, list) -> None
    """
    Trains Catboost classifier.

    :rtype: None
    :param x_train: features.
    :param y_train: labels.
    :param clfs: list of trained classifiers.
    :return: None.
    """
    pipeline = EnsembleVoteClassifier(clfs=clfs, voting="soft", weights=config.ensemble_weights, refit=False)
    pipeline.fit(x_train, y_train)
    model_name = os.path.join(config.models_folder, "ensemble-main.pickle")
    joblib.dump(pipeline, model_name, compress=1)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    sdb = sqlite.get_connection()
    data = sqlite.read_data(sdb, config.matches_table)
    data = data.dropna(axis=0, how="any")
    data, test_data = train_test_split(data, test_size=config.test_size, shuffle=False, random_state=config.seed)
    x, y = data[config.train_features].values, 1 - data["result"].values
    clf1 = train_catboost(x, y)
    clf2 = train_gnb(x, y)
    clf3 = train_svm(x, y)
    train_ensemble(x, y, clfs=[clf1, clf2, clf3])


if __name__ == "__main__":
    main()
