## Overview
Folder contains classification scripts together with supporting classes.

## BanyanSearch
### Description

BanyanSearch (from "banyan", Asian tree with multiple roots) is an automated float parameters tuning algorithm using RandomSearch, GridSearch and cross-validation.

The algorithm steps are the following: 

1.  BanyanSearch takes a list of parameters (multidimensional possible);
2.  Performs RandomSearch with `random_iter_pre` steps;
3.  Takes `random_best_pre` best (regarding `scoring` metric) parameters;
4.  Builds new parameters ranges from these best with the size = `set_size`;
5.  Performs GridSearch for each of these ranges multiple times expanding a tree regarding `tree_depth` and `tree_leafs` (number of leafs for each new layer);
6.  For each of found best parameters in leafs builds new detailed parameters ranges;
7.  Performs RandomSearch with `random_iter_post` steps;
8.  Takes `random_best_post` (default: 5) best parameters;

Every result at each step is accumulated, sorted and can be saved into text file.

Class is implemented as a separate module and located in `banyansearch.py`

### Parameters

Default parameters are:

```
cv_train = 2
set_size = 5
tree_depth = 4
tree_leafs = (2, 2, 1, 1)
random_iter_pre = 100
random_best_pre = 5
random_iter_post = 5
random_best_post = 5
```

### Scheme

<p align="center">
  <img src="/uploads/82547c5af9f8577f49f44d060cbea103/banyan_scheme.jpg" width="600px" >
</p>

### Usage

> x, y: train features (texts) and labels.

```
pca_params = {"svd_solver": "full"}
pipeline = Pipeline([
    ("scaler", StandardScaler()),
    ("pca", PCA(**pca_params)),
    ("clf", SVC(random_state=0))
])
param_grid = {"clf__C": np.geomspace(0.0001, 100, num=10000),
              "clf__gamma": np.geomspace(0.001, 10, num=10000),
              "pca__n_components": np.linspace(0.1, 0.999, num=100)}
restrictions = {"clf__C": [1e-5, 100], "clf__gamma": [1e-5, 100], "pca__n_components": [0.01, 0.9999]}
banyan = BanyanSearch(x, y, pipeline, restrictions, scoring, cv=config.cv_train, banyan_params=("clf__C", "clf__gamma", "pca__n_components"))
banyan.log_num_searches()
banyan.run_search(param_grid)
banyan.save_search_results(folder)
```
