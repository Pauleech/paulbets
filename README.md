# @paulbets

### Beating the Football Bookies with Machine Learning

## Tags

`football forecasting`, `gambling`, `machine learning`, `python`, `web scraping`, `lxml`, `scrapy`, `statistical modelling`, `mcmc`, `pystan`, `data mining`, `feature generation`, `sequence matching`, `sqlite`

## Introduction

Betting on sports is a popular pastime. Countless people all over the world enjoy sports betting, so much that they collectively spend billions of dollars on it each year. Not only because of fun, but there is also the chance to win money. The same could be said for almost all forms of gambling though, and few (if any) are as popular as sports betting. Fortunately, football is one of the most popular sports in the world, offers a huge amount of data to build a prediction model for betting.

The goal of this project is to predict match outcomes of European football matches more accurately than [Bet365](https://www.bet365.com/#/HO/) bookkeeper, thereby beating the odds and to, in the end, generate a positive return on investment. Based on the dataset provided on [Football-Data.co.uk](http://www.football-data.co.uk/) that includes basic match and bookkeeper data and FIFA players/clubs attributes scraped from [SoFIFA](https://sofifa.com/), a model to predict the probability of each match outcome was developed. Different betting strategies based on the built model are created and tested in order to validate the model performance.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes. 

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.0 Python 2.7 version;
* Scrapy 1.4: Web Crawling Framework for Python;
* PyStan: The Python Interface to Stan;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── matches                             # historical matches events and odds data
    ├── src                                     # project source
        ├── bayesball                           # dynamic hierarchical model for the prediction of current teams forms
        ├── betting                             # betting approaches
            ├── history                         # betting history results
            ├── ...
        ├── collecting                          # web crawling scripts
            ├── clubs                           # clubs data crawling scripts
            ├── matches                         # matches data scripts
                ├── matches                     # scrapy project
                    ├── spiders                 # scrapy spiders
                    ├── ... 
                ├── ... 
            ├── players                         # players data crawling scripts
        ├── mining                              # data mining and feature generation scrips
        ├── modules                             # additional modules of a common purpose
        ├── prediction                          # matches outcome prediction approaches
            ├── models                          # trained models
            ├── params                          # params tuning results
            ├── ...
    ├── config.py
    ├── ...    
    
`/src`, `/src/bayesball` and `/src/collecting/matches` have to be marked in PyCharm as sources root folder.

Folder `/src/prediction` contains its own README.md file.

### Glossary

* [Bet365](https://www.bet365.com/#/HO/) – a gambling company based in the United Kingdom. Bet365 is one of the world's leading online gambling groups with over 19 million customers in almost two hundred countries. The Group employs over 3,000 people and is the largest private employer in the city of Stoke-on-Trent.

* [Football-Data.co.uk](http://www.football-data.co.uk/) – a free football betting portal providing historical results & odds to help football betting enthusiasts analyse many years of data quickly and efficiently to gain an edge over the bookmaker. Whilst other football results and odds databases do exist, Football-Data is unique in making available computer-ready data in Excel and CSV format for quantitative analysis. 

* [SoFIFA](https://sofifa.com/) – a free EA Sports FIFA players and teams attributes portal. It provides these attributes with so-called versions, which are updated very often.

### Data

Project needs historical matches events and odds data from Football-Data.co.uk.  
It can be downloaded [here](https://yadi.sk/d/N4Errnb73NZfea) and supposed to be kept in `/data/matches` folder.

Filenames have to coincide with the leagues names from SoFIFA in order to allow correct data mining.

## Running the project

### Attributes crawling

Script `/src/collecting/main.py` launches FIFA players/clubs attributes scraping from SoFIFA. The data is saved into SQLite database.  

<p align="center">
  <img src="/uploads/d3d9885eb1f0e129cc1486803bbf1ffd/sofifa.png" width="600px" >
</p>

The earliest SoFIFA data version is `f12-156820` can be changed in `config.py`.

Data is scrapped via `requests` + `lxml` parallel approach together with highly-anonymous free proxy wrapping.

#### Usage

Scrap all the data.

```
python main.py scrap
```

Scrap random small amount of data in debug purposes.

```
python main.py scrap --debug_mode True
```

### Events crawling

Script `/src/collecting/matches/main.py` launches matches historical events scraping from [EnetScores](http://football-data.mx-api.enetscores.com/). This data contains teams lineups and substitutions together with all the matches events for the top leagues. The data is saved into JSON file. 

<p align="center">
  <img src="/uploads/25f08ad83f400145cde647314f38371d/enetscores.png" width="600px" >
</p>

Data is scrapped via scrapy parallel spiders. 

This approach is not used in the final model and therefore deprecated.

### MCMC Forms

Football teams abilities change over time due to various changes: from the squad to formation and tactics.

A hierarchical dynamic Bayesian model is provided in `/src/bayesball` folder for predicting the current teams forms regarding their previous performance. The MCMC NUTS algorithm is used to solve the inverse Bayesian problem of finding such abilities.

<p align="center">
  <img src="/uploads/37b8e80cc10242d7686b9b9946397739/bayesball.png" width="600px" >
</p>

Used model is described [here](http://mc-stan.org/events/stancon2017-notebooks/stancon2017-kharratzadeh-epl.pdf) in detail.

The forms are calculated via PyStan in the data mining section.

### Data mining

Script `/src/collecting/mining/main.py` launches FIFA players/clubs attributes matching and data mining together with features generation.

FIFA players/clubs attributes are matched using SequenceMatcher.

The data is saved into SQLite database. 

### Training the classifier

Project contains three types of classification models: support vector machine, naive Bayes model and CatBoost gradient boosting on decision trees model.  
Parameters can be tuned via BanyanSearch method.

To read more about parameters tuning, please visit [this](src/prediction) folder.

Script `/src/prediction/main.py` trains and saves the pipelines containing preprocessing techniques together with a classifier. Training model is supposed to be saved in `/src/prediction/models` folder.

The performances of classification models are tested using TimeSeriesSplit validation with 5 splits on 100% of the data.

<p align="center">
  <img src="/uploads/2f3c4baf0b3cdd6e1360b74858fae4d0/timesplit.png" width="600px" >
</p>

The best performance has been found using a catboost algorithm with some customized parameters:

```
confusion matrix (home, draw, away):
[[ 39.653   0.      5.38 ]
 [ 21.309   0.      5.583]
 [ 17.023   0.     11.052]]
 
folds accuracies: [ 50.209  50.478  48.924  50.478  53.437]
accuracy: 50.705%
```

Other algorithms have shown a bit less precise results.

It is close to the result of the Bet365 bookmaker's predictions. Sometimes the bookmaker provides the same odds for two events. In this case, the outcome is considered random between them.
On the same sample of data, the bookmaker has the following performance:

```
confusion matrix (home, draw, away):
[[ 38.822   0.036   6.175]
 [ 20.424   0.102   6.366]
 [ 16.258   0.018  11.799]]

folds accuracies: [ 50.239  50.568  48.655  50.448  53.706]
accuracy: 50.723%
```

It is also possible to check the performance of the voting algorithm. The algorithm takes all three models with some weights to predict the final outcome. But the performance of single catboost model is still better.

### Search betting parameters

Project contains two types of betting algorithms: expected value (EV) and Kelly criterion algorithms.

Using the trained classification model, it is possible to find the optimal betting parameters. For this, the data is divided according to the following convention: 30% of the data is used to train the original model with different classifier parameters. After that, the next 20% of the data is used to find the optimal betting parameters via grid search.  
 
After this, the model is trained on these 50% of the joint data. The remaining 50% of the data is used to verify the performance of the model. This division is quite unusual, but since the data is sufficient for training, most of the data is left for testing.  

<p align="center">
  <img src="/uploads/0d6f0d06c5b816f68b93244319489218/bet_search.png" width="600px" >
</p>

The data is divided in series, according to time intervals.

### Making bets

To make bets, the catboost algorithm is used. Script `/src/prediction/test_kelly.py` simulates making bets with Kelly algorithm using prediction model trained on the 50% of the data and found betting parameters. The results of betting procedure are saved into `/src/betting/history` folder.

The model has shown ambivalent performance coupled with the chosen betting algorithm and some tuned parameters. From the results it can be seen that the model can find inaccuracies in the odds of the bookmakers and play against them, especially in the game for outsiders, but also mistakes itself.

```
period: from 07-02-2015 to 11-06-2017
period days: 859
kelly bet: from €1 to ~€14 

income: ~€62
num_bets: 227
num_matches: 10039
accuracy: 25.110%
rate: 0.006
r^2: 0.808
```

To assess the quality of the growth of the balance, it is proposed to consider two parameters: `rate` and `r^2`. 
* `rate` is the slope of the linear regression, constructed from the `[match_num, current balance]` points. 

* `r^2` is the coefficient of determination, denoting the proportion of the variance in the dependent variable(s).

Finally, it is possible to observe the resulting return on investment.

<p align="center">
  <img src="/uploads/1b97e20d87ef0161cec5f2e56397d11c/catboost-kelly-main.png" width="600px" >
</p>

To summarize, it should be noted that the data is not enough to say that the algorithm will behave the same way in the long run. Unfortunately, such methods as bootstrapping can not be used in this case, since it is necessary to keep the balance of the teams' forces in the leagues throughout the season.
It should also be noted that with the availability of data from [opta](http://www.optasports.com) it would be possible to achieve better results. In addition, recently became popular models, the so-called expected goals models, which have already proved their worth, they could also improve the quality of the used model.