import os
import sys
import logging
import platform
import numpy as np
import multiprocessing
from modules.logering import setup_logger
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding("utf-8")
sys.stdout = stdout
np.random.seed(0)
np.set_printoptions(precision=3, suppress=True)

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_folder = os.path.join(root_path, "data")
matches_folder = os.path.join(root_path, "data/matches")
bayesball_path = os.path.join(root_path, "src/bayesball")
models_folder = os.path.join(root_path, "src/prediction/models")
prediction_params_folder = os.path.join(root_path, "src/prediction/params")
betting_history_folder = os.path.join(root_path, "src/betting/history")

# seed
seed = 42

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "paulbets.log")
logger = setup_logger("logger", log_name, log_level)

# sqlite db
db_name = os.path.join(root_path, "data.sqlite")
players_attr_table = "players_attr"
clubs_attr_table = "clubs_attr"
players_table = "players"
clubs_table = "clubs"
matches_table = "matches"
search_ev_table = "search_ev"
search_kelly_table = "search_kelly"

# web links
sofifa_website = "https://sofifa.com"
sofifa_players = "https://sofifa.com/players?offset={}"
sofifa_clubs = "https://sofifa.com/teams?offset={}"

# browsing delay
browsing_delay = 0.3
browsing_workers = 15
browsing_retrying = 10

# sofifa
sofifa_prev_version_class = "f12-156644"
sofifa_init_version_href = "?v=12&e=156644&set=true"
sofifa_statuses = ["added", "updated", "removed"]
sofifa_positions = ["RS", "ST", "LS", "RW", "LW", "RF", "CF",
                    "LF", "RAM", "CAM", "LAM", "RCM", "CM",
                    "LCM", "RM", "LM", "RDM", "CDM", "LDM",
                    "RCB", "CB", "LCB", "RB", "LB", "RWB", "LWB", "GK"]
sofifa_position_categories = {"RS": "att", "ST": "att", "LS": "att", "RW": "att",
                              "LW": "att", "RF": "att", "CF": "att", "LF": "att",
                              "RAM": "mid", "CAM": "mid", "LAM": "mid", "RCM": "mid",
                              "CM": "mid", "LCM": "mid", "RM": "mid", "LM": "mid",
                              "RDM": "mid", "CDM": "mid", "LDM": "mid",
                              "RCB": "def", "CB": "def", "LCB": "def", "RB": "def",
                              "LB": "def", "RWB": "def", "LWB": "def", "GK": "gk"}
sofifa_player_stats = ["PAC", "SHO", "PAS", "DRI", "DEF", "PHY"]
sofifa_player_stats_vis = ["stat{}".format(i) for i in range(1, 7)]
sofifa_player_wrs = {"High": 3, "Medium": 2, "Low": 1}
sofifa_club_stats = ["b_spd", "b_pas",
                     "c_pas", "c_cro", "c_sho",
                     "d_pre", "d_agg", "d_wid"]

# matches
home_away = ["home", "away"]
clubs_conv = {"Verona": "Hellas Verona",
              "Bergen": "Mons",
              "Germinal": "Beerschot AC",
              "Wolves": "Wolverhampton",
              "Arles": "AC Arles-Avignon",
              "Creteil": "US Creteil-Lusitanos",
              "Nijmegen": "NEC",
              "Sp Lisbon": "Sporting CP",
              "Murcia": "Real Murcia Club de Futbol",
              "UCAM Murcia": "Universidad Catolica de Murcia CF"}

# mining
matches_columns = ["home_team", "away_team", "league", "season", "date",
                   "B365H", "B365D", "B365A", "BbAvH", "BbAvD", "BbAvA", "BbMxH", "BbMxD", "BbMxA",
                   "home_goal", "away_goal"]
matches_rename = {"HomeTeam": "home_team", "AwayTeam": "away_team",
                  "FTHG": "home_goal", "FTAG": "away_goal"}
mining_slide = 5
mining_to_means = ["goal", "form", "win", "draw", "loss"]

# banyan parameters
cv_train = 5
banyan_set_size = 5
banyan_tree_depth = 3
banyan_tree_leafs = (2, 1, 1)
banyan_random_iter_pre = 100
banyan_random_best_pre = 5
banyan_random_iter_post = 5
banyan_random_best_post = 5

# model parameters
train_features = ["BbAvH", "BbAvD", "BbAvA", "BbMxH", "BbMxD", "BbMxA",
                  "home_position", "away_position", "diff_points", "home_form", "away_form",
                  "home_goal_a5", "away_goal_a5", "home_form_a5", "away_form_a5",
                  "home_win_a5", "away_win_a5", "home_draw_a5", "away_draw_a5",
                  "home_loss_a5", "away_loss_a5", "home_goal_p5", "away_goal_p5",
                  "home_form_p5", "away_form_p5", "home_win_p5", "away_win_p5",
                  "home_draw_p5", "away_draw_p5", "home_loss_p5", "away_loss_p5",
                  "home_fresh", "away_fresh", "away_pot_att", "away_pot_def", "away_pot_gk", "away_pot_mid",
                  "away_rat_att", "away_rat_def", "away_rat_gk", "away_rat_mid", "home_pot_att",
                  "home_pot_def", "home_pot_gk", "home_pot_mid", "home_rat_att", "home_rat_def",
                  "home_rat_gk", "home_rat_mid", "diff_form", "diff_position", "home_hazard",
                  "away_hazard", "home_power", "away_power", "diff_power", "diff_fresh"]
train_size = 0.3
tune_size = 0.4
test_size = 0.5

# svm parameters
svm_c = 0.76419144660361804
svm_gamma = 0.01161476927592435

# dimred parameters
pca_evr = 0.47969302398989905

# catboost parameters
catboost_params = {"depth": 6, "random_seed": 42, "loss_function": "MultiClass",
                   "iterations": 100, "l2_leaf_reg": 50,
                   "border_count": 50, "learning_rate": 0.029221250250024998,
                   "ctr_border_count": 50, "allow_writing_files": False}

# ensemble parameters
ensemble_weights = (0.97073144277063284, 0.0038603515102610952, 0.1785799680576563)

# betting
cv_bet = 5
bet_random_tune = 1000
odds_cols = ["B365H", "B365D", "B365A"]

# ev betting
stake = 1.
min_ev = 0.2
max_odd = 10

# kelly betting
min_share = 0.07
kelly_stake = stake/min_share
